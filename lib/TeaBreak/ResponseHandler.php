<?php

namespace TeaBreak;

/**
 * HTTPレスポンスを管理するクラス
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class ResponseHandler
{

    /**
     * インスタンス
     * @var __CLASS__
     */
    protected static $instance;

    /**
     * コンストラクタ
     */
    private function __construct()
    {
        
    }

    /**
     * インスタンスを返す
     * @return __CLASS__
     */
    public static function getInstance()
    {
        if (\is_null(static::$instance)) {
            static::$instance = new static();
        }

        return static::$instance;
    }

}
