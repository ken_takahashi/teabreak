<?php

namespace TeaBreak\Plugins;

require_once 'PluginBase.php';

class LoadDataPlugin extends DbPluginBase
{

    /**
     * Smartyから呼び出されるメソッド
     * 
     * $params は連想配列で取りうるキーは
     *  - loadClass = DataModelクラス名、ORM用クラス、必須
     *  - dbh = データベースの識別名、オプション（指定しない場合はDefault設定されているDBConfigが使用される）
     *  - assign = 検索結果を納めるテンプレート変数名、オプション（指定しない場合にはloadClassで指定された値が使用される）
     * 
     * @param array $params
     * @param \Smarty $template
     * @throws \TeaBreak\Exception\LogicException
     */
    public function pluginMethod($params, $template)
    {
        // Database Handler の取得
        $dbh = null;
        if (isset($params['dbh'])) {
            $dbh = $this->getDbh($params['dbh']);
        } else {
            $firstDbh = null;
            foreach ($this->_config->database->getAllAttributes() as $dbconf) {
                if ($dbconf->default === TRUE) {
                    $dbh = $this->_dbh[$dbconf->name];
                    break;
                }
                if (is_null($firstDbh)) {
                    $firstDbh = $this->_dbh[$dbconf->name];
                }
            }

            if (is_null($dbh) && is_null($dbconf)) {
                throw new \TeaBreak\Exception\LogicException('undefined database handler. [' . __FUNCTION__ . ']');
            } elseif (is_null($dbh)) {
                $dbh = $firstDbh;
            }
        }
        if (is_a($dbh, 'PDO') === FALSE) {
            throw new \TeaBreak\Exception\LogicException('Invalid database handler. [' . __FUNCTION__ . ']');
        }

        // ORMオブジェクトの取得
        $loadClass = $params['loadClass'];
        if (include_once('models/' . $loadClass . '.php')) {
            if (class_exists($loadClass)) {
                $obj = new $loadClass($dbh);
            }
        }
        if (is_a($obj, '\TeaBreak\Database\DataMapper') === FALSE) {
            throw new \TeaBreak\Exception\LogicException('Invalid ORM Class. ' . $loadClass);
        }

        // データ取得
        $result = null;
        if (isset($params['find'])) {
            $form = $this->_request->getInputArray($params['find']);
            if ($form) {
                $result = $obj->find($form);
            } else {
                $result = $obj->find_all();
            }
        } else {
            $result = $obj->find_all();
        }

        // テンプレートへアサイン
        if (isset($params['assign'])) {
            $template->assign($params['assign'], $result);
        } else {
            $template->assign($loadClass, $result);
        }
    }

}
