<?php

namespace TeaBreak\Plugins;

require_once 'PluginBase.php';

abstract class DbPluginBase extends PluginBase
{

    protected $_dbh;

    /**
     * コンストラクタ
     */
    public function __construct()
    {
        parent::__construct();

        $this->_dbh = array();
        foreach ($this->_config->database->getAllAttributes() as $dbconf) {
            $dbh = \TeaBreak\Database\DatabaseHandler::getInstance($dbconf);
            $this->_dbh[$dbconf->name] = $dbh;
        }
    }

    /**
     * データベースハンドラを取得する
     * @param string $name
     * @return PDO
     * @throws Exception\LogicException
     */
    public function getDbh($name)
    {
        if (isset($this->_dbh[$name])) {
            return $this->_dbh[$name];
        } else {
            throw new \TeaBreak\Exception\LogicException('undefined database handler. [' . __FUNCTION__ . ']');
        }
    }

}
