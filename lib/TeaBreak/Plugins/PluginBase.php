<?php

namespace TeaBreak\Plugins;

/**
 * Smarty Plugin 用の基底クラス
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
abstract class PluginBase
{

    /**
     * アプリケーション設定オブジェクト
     * @var \TeaBreak\ConfigurationHandler
     */
    protected $_config;

    /**
     * リクエストハンドラオブジェクト
     * @var \TeaBreak\RequestHandler
     */
    protected $_request;

    /**
     * コンストラクタ
     */
    public function __construct()
    {
        $this->_config = \TeaBreak\ConfigurationHandler::getInstance();
        $this->_request = \TeaBreak\RequestHandler::getInstance();
    }

    abstract public function pluginMethod($params, $template);
}
