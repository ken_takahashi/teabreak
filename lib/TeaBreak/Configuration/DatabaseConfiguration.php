<?php

namespace TeaBreak\Configuration;

/**
 * Database用設定のコンテナクラス
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class DatabaseConfiguration extends AbstractConfigurationCollection
{

    const ERROR_TYPE_INVALID = 'type invalid';
    const CHOSE_CLASS_ATTR = 'driver';
    const CHOSE_CLASS_VALIDATOR = '_isValidDriver';

    /**
     * 与えられた値がプロパティ driver の値として妥当かを検査する
     * @param mixed $var
     * @return boolean
     */
    protected function _isValidDriver($value)
    {
        if (\is_string($value) && empty($value) === \FALSE) {
            if (\in_array($value, \PDO::getAvailableDrivers())) {
                return \TRUE;
            } else {
                return \FALSE;
            }
        } else {
            return \FALSE;
        }
    }

}
