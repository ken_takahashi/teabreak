<?php

namespace TeaBreak\Configuration;

use TeaBreak\Exception;

/**
 * 設定用の基底クラス
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 * @abstract
 */
abstract class Configuration
{

    /**
     * プロパティ名: name
     * @var string
     */
    const PROPERTY_NAME = 'name';

    /**
     * 設定保持用変数
     * @var array
     */
    protected $_conf;

    /**
     * プロパティが read only かのフラグ
     * @var boolean
     */
    protected $_readonly;

    /**
     * validationエラー保持用変数
     * @var array
     */
    protected $_error = array();

    /**
     * 識別名
     * @var PropertyTypeString
     */
    protected $name;

    /**
     * プロパティの型設定
     */
    protected function _setPropertyType()
    {
        $this->name = new PropertyType\String(static::PROPERTY_NAME, \TRUE);
    }

    /**
     * コンストラクタ
     *
     * 設定項目の妥当性を検査しつつ、設定を保持する。
     *
     * @param  string $name
     * @param  array $conf
     * @param  boolean $readonly
     * @throws Exception\ConfigurationException
     */
    public function __construct($name, $conf, $readonly = \TRUE)
    {
        // メンバの初期化
        $this->_setPropertyType();

        // 引数チェック
        if (\is_string($name) === \FALSE) {
            throw new Exception\ConfigurationException('invalid paramater. $name is not string');
        }

        if (\is_array($conf) === \FALSE) {
            throw new Exception\ConfigurationException('invalid paramater. $conf is not array');
        }

        // name のチェックと代入
        if ($this->name->isValid($name)) {
            $this->name->value = $name;
        } else {
            throw new Exception\ConfigurationException('invalid paramater. $name = ' . $name);
        }
        $this->_conf = $conf;

        // readonly フラグを一時的にFALSE固定にする
        $this->_readonly = \FALSE;

        // name 以外のメンバの代入
        $obj = new \ReflectionObject($this);
        foreach ($obj->getProperties() as $property) {
            // name と _ で始まるメンバは除外（_で始まる変数は内部でしか使わない）
            if ($property->getName() !== 'name' && \substr($property->getName(), 0, 1) !== '_') {
                $key = $property->getName();
                $value = isset($conf[$key]) ? $conf[$key] : \NULL;
            } else {
                continue;
            }

            try {
                $this->__set($key, $value);
            } catch (\TeaBreak\Exception\ConfigurationException $e) {
                $this->_error[] = $e->getMessage();
            }
        }

        // readonly フラグを設定
        $this->_readonly = $readonly;

        // エラーがあったら例外をスローする
        if (empty($this->_error) === \FALSE) {
            throw new Exception\ConfigurationException('invalid paramater. ' . \var_export($this->_error, \TRUE));
        }
    }

    /**
     * property getter
     *
     * プロパティの値を取得する。
     * @param  string $name
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function __get($name)
    {
        if (\is_string($name) === \FALSE) {
            throw new \InvalidArgumentException;
        }

        if (isset($this->$name)) {
            return $this->$name->value;
        } else {
            return \NULL;
        }
    }

    /**
     * property setter
     *
     * プロパティの値を設定する。
     * ただし、デフォルトでは read only。
     * @param  string $name
     * @param  mixed $value
     * @throws Exception\ConfigurationException
     */
    public function __set($name, $value)
    {
        if ($this->_readonly) {
            throw new Exception\ConfigurationException('property is read only.');
        }

        // 依存関係など型の妥当性以外の Varidator がある場合にはそれを実行する
        $validator = '_isValid' . ucfirst($name);
        if (method_exists($this, $validator) && $this->$validator($value) === FALSE) {
            throw new Exception\ConfigurationException('invalid paramater. ' . $name);
        }

        if ($this->$name->isValid($value) === FALSE) {
            throw new Exception\ConfigurationException('property is invalid. : ' . $name);
        }

        $this->$name->value = $value;
    }

    /**
     * method caller
     *
     * 未定義のメソッドがコールされたら例外を返す。
     * @param  string $name
     * @param  array $arguments
     * @throws \BadMethodCallException
     */
    public function __call($name, $arguments)
    {
        throw new \BadMethodCallException(sprintf('%s is undefined method.', $name));
    }

    /**
     * 設定項目が妥当かを検査する
     *
     * @return boolean
     */
    public function isValid()
    {
        $obj = new \ReflectionObject($this);
        foreach ($obj->getProperties() as $property) {
            if ($this->$property->isValid($this->_conf[$property]) === FALSE) {
                $this->_error[] = 'property is invalid. : ' . $property;
            }
        }

        if (empty($this->_error)) {
            return \TRUE;
        } else {
            return \FALSE;
        }
    }

    /**
     * エラーを取得する
     * @return array
     */
    public function getError()
    {
        return $this->_error;
    }

}
