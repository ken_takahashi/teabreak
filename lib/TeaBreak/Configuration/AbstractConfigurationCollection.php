<?php

namespace TeaBreak\Configuration;

/**
 * Database用設定のコンテナクラス
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
abstract class AbstractConfigurationCollection extends Configuration
{

    /**
     * コンストラクタ
     * @param string $name
     * @param array $conf
     */
    public function __construct($name, array $conf)
    {
        parent::__construct($name, $conf);

        $choseClassAttr = static::CHOSE_CLASS_ATTR;
        $validator = static::CHOSE_CLASS_VALIDATOR;
        $classPostfix = \str_replace(__NAMESPACE__ . '\\', '', \get_called_class());
        $this->_conf = array();
        foreach ($conf as $key => $var) {
            $attr = $var[$choseClassAttr];
            if (isset($attr) && $this->$validator($attr)) {
                $className = '\\' . __NAMESPACE__ . '\\' . \ucfirst($attr) . $classPostfix;
                $this->_conf[$key] = new $className($key, $var);
            }
        }
    }

    /**
     * getter
     * @param string $name
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function __get($name)
    {
        if (is_string($name) === FALSE) {
            throw new \InvalidArgumentException;
        }

        if (isset($this->$name)) {
            return $this->$name;
        } elseif (isset($this->_conf[$name])) {
            return $this->_conf[$name];
        } else {
            return \NULL;
        }
    }

    /**
     * 要素名のリストを取得する
     */
    public function getAllAttributeNames()
    {
        return array_keys($this->_conf);
    }

    /**
     * 要素のリストを取得する
     */
    public function getAllAttributes()
    {
        return $this->_conf;
    }

}
