<?php

namespace TeaBreak\Configuration;

/**
 * SQLite用設定ファイル
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class SqliteDatabaseConfiguration extends AbstractDbConfiguration
{

    /**
     * path用変数名
     * @var string
     */
    const PROPERTY_PATH = 'path';

    /**
     * データベースパス
     * @var string
     */
    protected $path;

    protected function _setPropertyType()
    {
        parent::_setPropertyType();

        $this->path = new PropertyType\Path('path', TRUE);
        $this->path->required_exist = FALSE;
    }

    /**
     * DSNを返す
     * @return string
     */
    protected function _getDSN()
    {
        if (isset($this->dsn->value) === \FALSE) {
            $this->dsn->value = \sprintf('%s:%s', $this->driver->value, $this->path->value);
        }

        return $this->dsn->value;
    }

}
