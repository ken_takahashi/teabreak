<?php

namespace TeaBreak\Configuration;

/**
 * Staticタイプのルーティング設定用クラス
 *
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class StaticRoutingConfiguration extends Configuration implements InterfaceRoutingConfiguration
{

    const VAR_ACCESS = 'access';
    const VAR_TYPE = 'type';
    const VAR_OPTIONS = 'options';

    protected $access;
    protected $type;
    protected $options = array();

    protected function _setPropertyType()
    {
        parent::_setPropertyType();

        $this->access = new PropertyType\String(static::VAR_ACCESS, TRUE);
        $this->type = new PropertyType\String(static::VAR_TYPE, TRUE);
        $this->options = new PropertyType\Collection(static::VAR_OPTIONS);
        $this->options->elements_type = PropertyType\Collection::ELEMENTS_TYPE_MIXED;
    }

}
