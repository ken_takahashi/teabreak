<?php

namespace TeaBreak\Configuration;

class RoutingConfiguration extends AbstractConfigurationCollection
{

    const ERROR_TYPE_INVALID = 'type invalid';
    const CHOSE_CLASS_ATTR = 'type';
    const CHOSE_CLASS_VALIDATOR = '_isValidType';

    private static $_typeList = array(
        'page',
        'static',
        'restapi',
    );

    /**
     * 与えられた値がプロパティ type の値として妥当かを検査する
     * @param mixed $var
     * @return boolean
     */
    protected function _isValidType($var)
    {
        if (in_array($var, static::$_typeList)) {
            return \TRUE;
        } else {
            $this->_error[] = array(__FUNCTION__, static::ERROR_TYPE_INVALID);
            return \FALSE;
        }
    }

    /**
     * ページの設定を取得する
     * @param string $path
     * @return \TeaBreak\Configuration\PageRoutingConfiguration
     * @throws \TeaBreak\Exception\ConfigurationException
     */
    public function getPage($path)
    {
        if ($path === '') {
            $path = '/';
        }

        if (in_array($path, array_keys($this->_conf))) {
            return $this->_conf[$path];
        } else {
            foreach (array_keys($this->_conf) as $key) {
                if ($key === '/') {
                    continue;
                }
                $pattern = '|^' . preg_quote($key) . '.*$|';
                if (preg_match($pattern, $path)) {
                    return $this->_conf[$key];
                }
            }
            throw new \TeaBreak\Exception\ConfigurationException($path . ' is not defined.');
        }
    }

}
