<?php

namespace TeaBreak\Configuration;

/**
 * 基本設定用クラス
 * 
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class CommonConfiguration extends \TeaBreak\Configuration\Configuration
{

    const VAR_COMPILE_DIR = 'mailaddr';

    /**
     * サイト管理者のメールアドレス
     * @var $mail_addr
     */
    protected $mailaddr;

    protected function _setPropertyType()
    {
        parent::_setPropertyType();

        $this->mailaddr = new PropertyType\Path(static::VAR_COMPILE_DIR, \TRUE);
        $this->mailaddr->required_exist = TRUE;
    }

}
