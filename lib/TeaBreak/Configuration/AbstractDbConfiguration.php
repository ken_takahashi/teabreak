<?php

namespace TeaBreak\Configuration;

use TeaBreak\Exception;

/**
 * Database用設定クラスの抽象クラス
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 * @abstract
 */
abstract class AbstractDbConfiguration extends Configuration
{

    /**
     * ドライバー用変数名
     * @var string
     */
    const PROPERTY_DRIVER = 'driver';

    /**
     * ドライバーオプション用変数名
     * @var string
     */
    const PROPERTY_OPTIONS = 'options';

    /**
     * DSN用変数名
     * @var string
     */
    const PROPERTY_DSN = 'dsn';

    /**
     * ドライバー
     * @var string
     */
    protected $driver;

    /**
     * DSN
     * @var string
     */
    protected $dsn;

    /**
     * ドライバーオプション
     * @var array
     */
    protected $options;

    protected function _setPropertyType()
    {
        parent::_setPropertyType();

        $this->driver = new PropertyType\PdoDriver(static::PROPERTY_DRIVER, \TRUE);
        $this->dsn = new PropertyType\String(static::PROPERTY_DSN, \FALSE);
        $this->options = new PropertyType\Collection(static::PROPERTY_OPTIONS, \FALSE);
        $this->options->elements_type = 'String';
    }

    /**
     * getter
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        if ($name === static::PROPERTY_DSN) {
            return $this->_getDSN();
        } else {
            return parent::__get($name);
        }
    }

    /**
     * DSNを返す
     * @return string
     * @abstract
     */
    abstract protected function _getDSN();

    /**
     * 与えられた値がプロパティ options の値として妥当かを検査する
     * @param mixed $var
     * @return boolean
     */
    protected function _isValidOptions($var)
    {
        if (\is_null($var) || empty($var)) {
            return \TRUE;
        } elseif (\is_array($var) === \FALSE) {
            $this->_error[] = 'invalid property value: options = ' . \var_export($var, \TRUE);
            return \FALSE;
        } else {
            foreach ($var as $key => $value) {
                $obj = new \ReflectionClass('PDO');
                if (\array_key_exists($key, $obj->getConstants()) === \FALSE) {
                    $this->_error[] = 'invalid property value: options = ' . \var_export($var, \TRUE);
                    return \FALSE;
                }
            }

            return \TRUE;
        }
    }

}
