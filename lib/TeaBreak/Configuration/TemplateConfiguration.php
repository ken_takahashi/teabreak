<?php

namespace TeaBreak\Configuration;

/**
 * テンプレート設定用クラス
 * 
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class TemplateConfiguration extends \TeaBreak\Configuration\Configuration
{

    const VAR_COMPILE_DIR = 'compile_dir';
    const VAR_TEMPLATE_DIR = 'template_dir';
    const VAR_PLUGINS_DIR = 'plugins_dir';
    const VAR_LEFT_DELIMITER = 'left_delimiter';
    const VAR_RIGHT_DELIMITER = ' right_delimiter';

    /**
     * テンプレートのコンパイルキャッシュ
     * @var $compile_dir
     */
    protected $compile_dir;

    /**
     * テンプレートのパス
     * @var string
     */
    protected $template_dir;

    /**
     * プラグインのパス
     * @var string
     */
    protected $plugins_dir;

    /**
     * デリミタ左
     * @var string
     */
    protected $left_delimiter;

    /**
     * デリミタ右
     */
    protected $right_delimiter;

    protected function _setPropertyType()
    {
        parent::_setPropertyType();

        $this->compile_dir = new PropertyType\Path(static::VAR_COMPILE_DIR, \TRUE);
        $this->compile_dir->required_exist = TRUE;

        $this->template_dir = new PropertyType\Collection(static::VAR_TEMPLATE_DIR, \TRUE);
        $this->template_dir->elements_type = 'Path';
        $this->template_dir->elements_type_options = array(PropertyType\Path::VAR_REQUIRED_EXIST => TRUE);

        $this->plugins_dir = new PropertyType\Collection(static::VAR_PLUGINS_DIR, \FALSE);
        $this->plugins_dir->elements_type = 'Path';
        $this->plugins_dir->elements_type_options = array(PropertyType\Path::VAR_REQUIRED_EXIST => TRUE);

        $this->left_delimiter = new PropertyType\String(static::VAR_LEFT_DELIMITER, FALSE);
        $this->right_delimiter = new PropertyType\String(static::VAR_RIGHT_DELIMITER, \FALSE);
    }

}
