<?php

namespace TeaBreak\Configuration;

/**
 * MySQL用設定クラス
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class MysqlDatabaseConfiguration extends AbstractDbConfiguration
{

    /**
     * プロパティ名: dbname
     * @var string
     */
    const PROPERTY_DBNAME = 'dbname';

    /**
     * プロパティ名: host
     * @var string
     */
    const PROPERTY_HOST = 'host';

    /**
     * プロパティ名: port
     * @var string
     */
    const PROPERTY_PORT = 'port';

    /**
     * プロパティ名: unix_socket
     * @var string
     */
    const PROPERTY_UNIX_SOCKET = 'unix_socket';

    /**
     * プロパティ名: username
     * @var string
     */
    const PROPERTY_USERNAME = 'username';

    /**
     * プロパティ名: password
     * @var string
     */
    const PROPERTY_PASSWORD = 'password';

    /**
     * プロパティ名: charset
     * @var string
     */
    const PROPERTY_CHARSET = 'charset';

    /**
     * データベース名
     * @var string
     */
    protected $dbname;

    /**
     * ホスト名
     * @var string
     */
    protected $host;

    /**
     * ポート番号
     * @var int
     */
    protected $port;

    /**
     * キャラクタセット
     * @var string
     */
    protected $charset;

    /**
     * UNIXソケットパス
     * @var string
     */
    protected $unix_socket;

    /**
     * ユーザー名
     * @var string
     */
    protected $username;

    /**
     * パスワード
     * @var string
     */
    protected $password;

    protected function _setPropertyType()
    {
        parent::_setPropertyType();

        $this->dbname = new PropertyType\String(static::PROPERTY_DBNAME, \TRUE);
        $this->host = new PropertyType\String(static::PROPERTY_HOST, \FALSE);
        $this->port = new PropertyType\Int(static::PROPERTY_PORT, \FALSE);
        $this->charset = new PropertyType\String(static::PROPERTY_CHARSET, \FALSE);
        $this->unix_socket = new PropertyType\Path(static::PROPERTY_UNIX_SOCKET, \FALSE);
        $this->unix_socket->required_exist = \TRUE;
        $this->username = new PropertyType\String(static::PROPERTY_USERNAME, \TRUE);
        $this->password = new PropertyType\String(static::PROPERTY_PASSWORD, \TRUE);
    }

    /**
     * DSNを返す
     * @return string
     */
    protected function _getDSN()
    {
        if (isset($this->dsn)) {
            return $this->dsn;
        }

        $dsn = \sprintf('%s:dbname=%s', $this->driver, $this->dbname);
        if (isset($this->unix_socket)) {
            $dsn = ':unix_socket=%s' . $this->unix_socket;
        } else {
            $dsn = ':host=%s' . $this->host;
            if (isset($this->port)) {
                $dsn .= ':port=' . $this->port;
            }
        }
        if (isset($this->charset)) {
            $dsn .= ':charset=' . $this->charset;
        }
        $this->dsn = $dsn;

        return $this->dsn;
    }

    protected function _isValidHost($var)
    {
        return $this->_isValidHostAndUnixSocket($var);
    }

    protected function _isValidUnix_socket($var)
    {
        return $this->_isValidHostAndUnixSocket($var);
    }

    protected function _isValidHostAndUnixSocket($var)
    {
        $host = isset($this->_conf[static::PROPERTY_HOST]) ? $this->_conf[static::PROPERTY_HOST] : NULL;
        $unix_socket = isset($this->_conf[static::PROPERTY_UNIX_SOCKET]) ? $this->_conf[static::PROPERTY_UNIX_SOCKET] : NULL;
        if ((empty($host) && empty($unix_socket)) || (empty($host) === \FALSE && empty($unix_socket) === \FALSE)) {
            return \FALSE;
        } else {
            return \TRUE;
        }
    }

}
