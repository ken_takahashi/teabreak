<?php

namespace TeaBreak\Configuration\PropertyType;

/**
 * 真偽型設定値用クラス
 *
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class Boolean extends AbstractPropertyType
{

    public function isValid($value)
    {
        if (\is_bool($value)) {
            return \TRUE;
        } else {
            return \FALSE;
        }
    }

}
