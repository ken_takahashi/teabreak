<?php

namespace TeaBreak\Configuration\PropertyType;

/**
 * 配列型設定値用クラス
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class Collection extends AbstractPropertyType
{

    /**
     * 配列の要素に何でも入る場合の特殊な値
     * @var string
     */
    const ELEMENTS_TYPE_MIXED = 'mixed';

    /**
     * 配列の要素の型を規定する変数名
     */
    const VAR_ELEMENTS_TYPE = 'elements_type';

    /**
     * 配列の要素の型のオプション
     * @var string
     */
    const VAR_ELEMENTS_TYPE_OPTIONS = 'elements_type_options';

    /**
     * 配列の要素の型
     * @var string 定義済みのクラス名
     */
    protected $elements_type;

    /**
     * 配列の要素の型のオプション
     * @var array オプション名をキーにした配列
     */
    protected $elements_type_options = array();

    public function __construct($name, $is_required = FALSE, $value = NULL)
    {
        parent::__construct($name, $is_required, $value);

        $this->__set(static::VAR_ELEMENTS_TYPE, static::ELEMENTS_TYPE_MIXED);
    }

    public function _customSetter($name, $value)
    {
        switch ($name) {
            case static::VAR_ELEMENTS_TYPE:
                if ($value === static::ELEMENTS_TYPE_MIXED || class_exists(__NAMESPACE__ . '\\' . $value)) {
                    $this->elements_type = $value;
                } else {
                    throw new \TeaBreak\Exception\ConfigurationException('unknown class. ' . $value);
                }
                break;

            case static::VAR_ELEMENTS_TYPE_OPTIONS:
                if (is_array($value) === FALSE) {
                    throw new \TeaBreak\Exception\ConfigurationException('element_type_options is not array.');
                }
                $this->elements_type_options = $value;
                break;

            default:
                parent::_customSetter($name, $value);
        }
    }

    public function _setValue($value)
    {
        if (\is_array($value) === FALSE) {
            $value = array($value);
        }

        $list = array();
        foreach ($value as $key => $val) {
            if (empty($val)) {
                continue;
            }
            if ($this->elements_type === static::ELEMENTS_TYPE_MIXED) {
                $list[] = $val;
            } else {
                $element_type = __NAMESPACE__ . '\\' . $this->elements_type;
                $elm = new $element_type($key);
                foreach ($this->elements_type_options as $opKey => $opVal) {
                    $elm->$opKey = $opVal;
                }
                $elm->__set(static::VAR_VALUE, $val);
                $list[$key] = $elm->value;
            }
        }
        $this->value = $list;
    }

    public function isValid($value)
    {
        if ($this->is_required && empty($value)) {
            // 必須なのにからなのでFALSE
            return \FALSE;
        } elseif ($this->is_required === \FALSE && empty($value)) {
            // 必須でなくてからなのでTRUE
            return \TRUE;
        }

        if ($this->elements_type === static::ELEMENTS_TYPE_MIXED) {
            // 配列に何が入っていてもいいのでTRUE
            return \TRUE;
        }

        // 配列じゃなかったら配列に変換
        if (\is_array($value) === \FALSE) {
            $value = array($value);
        }

        // 配列じゃなかったら配列に変換
        if (\is_array($value) === \FALSE) {
            $value = array($value);
        }

        // 配列の要素の型チェック
        foreach ($value as $key => $val) {
            $element_type = __NAMESPACE__ . '\\' . $this->elements_type;
            $elm = new $element_type($key, FALSE);
            if ($elm->isValid($val) === FALSE) {
                // 指定された方ではなかったのでFALSE
                return FALSE;
            }
        }

        // 全部通ったのでTRUE
        return \TRUE;
    }

}
