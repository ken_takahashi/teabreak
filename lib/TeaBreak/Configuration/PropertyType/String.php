<?php

namespace TeaBreak\Configuration\PropertyType;

/**
 * 文字列型設定値用クラス
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class String extends AbstractPropertyType
{

    public function isValid($value)
    {
        if ($this->is_required) {
            if (\is_scalar($value) && empty($value) === \FALSE) {
                return \TRUE;
            } else {
                return \FALSE;
            }
        } else {
            if (\is_scalar($value) || empty($value)) {
                return \TRUE;
            } else {
                return \FALSE;
            }
        }
    }

}
