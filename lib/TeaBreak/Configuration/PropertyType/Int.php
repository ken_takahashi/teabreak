<?php

namespace TeaBreak\Configuration\PropertyType;

/**
 * 整数型設定値用クラス
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class Int extends AbstractPropertyType
{

    public function isValid($value)
    {
        if ($this->is_required === FALSE && empty($value)) {
            return \TRUE;
        } else {
            if (is_int($value) || (is_scalar($value) && $value == (int) $value)) {
                return \TRUE;
            } else {
                return \FALSE;
            }
        }
    }

}
