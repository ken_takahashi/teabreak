<?php

namespace TeaBreak\Configuration\PropertyType;

/**
 * Path型設定値用抽象クラス
 *
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class Path extends AbstractPropertyType
{

    const VAR_REQUIRED_EXIST = 'required_exist';

    /**
     * 設定されたパスが存在していることを確認する
     * @var boolean
     */
    protected $required_exist;

    public function __construct($name, $is_required = FALSE, $value = NULL)
    {
        parent::__construct($name, $is_required, $value);

        $this->__set(static::VAR_REQUIRED_EXIST, \FALSE);
    }

    public function _setValue($value)
    {
        if ($this->isValid($value)) {
            if ($this->required_exist) {
                $this->value = $this->_normalizePaths($value);
            } else {
                $this->value = $value;
            }
        } elseif ($this->is_required) {
            throw new \TeaBreak\Exception\ConfigurationException('invalid $value.');
        }
    }

    public function isValid($value)
    {
        if ($this->is_required && (\is_string($value) === \FALSE || empty($value))) {
            return \FALSE;
        } elseif ($this->is_required === \FALSE && \is_string($value) === \FALSE && empty($value) === \FALSE) {
            return \FALSE;
        }

        if (($this->required_exist === FALSE) || ($this->required_exist && $this->_normalizePaths($value))) {
            return \TRUE;
        } else {
            return \FALSE;
        }
    }

    public function _customSetter($name, $value)
    {
        switch ($name) {
            case static::VAR_REQUIRED_EXIST:
                if (\is_bool($value)) {
                    $this->required_exist = $value;
                } else {
                    throw new \TeaBreak\Exception\ConfigurationException('invalid $required_exist. ' . $value);
                }
                break;

            default:
                parent::_customSetter($name, $value);
                break;
        }
    }

    /**
     * path を絶対パスに変換する
     * @param mixed $var
     * @return array
     */
    protected function _normalizePaths($var)
    {
        if (\realpath($var)) {
            $abs_path = \realpath($var);
        } elseif (\realpath(\APP_PATH . \DIRECTORY_SEPARATOR . $var)) {
            $abs_path = \realpath(\APP_PATH . \DIRECTORY_SEPARATOR . $var);
        } else {
            return \FALSE;
        }

        return $abs_path;
    }

}
