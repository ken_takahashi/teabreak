<?php

namespace TeaBreak\Configuration\PropertyType;

/**
 * POD ドライバー型設定値のクラス
 *
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class PdoDriver extends String
{

    /**
     * 与えられた値がプロパティ driver の値として妥当かを検査する
     * @param mixed $var
     * @return boolean
     */
    protected function _isValid($value)
    {
        parent::isValid($value);

        if (\is_string($value) && empty($value) === \FALSE) {
            if (\in_array($value, \PDO::getAvailableDrivers())) {
                return \TRUE;
            } else {
                return \FALSE;
            }
        } else {
            return \FALSE;
        }
    }

}
