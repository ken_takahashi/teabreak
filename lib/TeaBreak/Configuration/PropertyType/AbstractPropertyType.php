<?php

namespace TeaBreak\Configuration\PropertyType;

/**
 * 設定値用基底クラス
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
abstract class AbstractPropertyType
{

    const VAR_NAME = 'name';
    const VAR_IS_REQUIRED = 'is_required';
    const VAR_VALUE = 'value';

    /**
     * プロパティの名前
     * @var string
     */
    protected $name;

    /**
     * プロパティの値
     * @var mixed
     */
    protected $value;

    /**
     * 必須項目フラグ
     * @var boolean
     */
    protected $is_required;

    /**
     * コンストラクタ
     * @param string $name
     * @param boolean $is_required
     * @param mixed $value
     */
    public function __construct($name, $is_required = FALSE, $value = NULL)
    {
        $this->__set(static::VAR_NAME, $name);
        $this->__set(static::VAR_IS_REQUIRED, $is_required);
        if (is_null($value) === FALSE) {
            $this->__set(static::VAR_VALUE, $value);
        }
    }

    /**
     * getter
     * @param string $name
     * @return mixed
     * @throws \TeaBreak\Exception\ConfigurationException
     */
    public function __get($name)
    {
        $obj = new \ReflectionClass($this);
        if ($obj->getProperty($name)) {
            return $this->$name;
        } else {
            throw new \TeaBreak\Exception\ConfigurationException;
        }
    }

    /**
     * setter
     * @param string $name
     * @param mixed $value
     * @throws \TeaBreak\Exception\ConfigurationException
     */
    public function __set($name, $value)
    {
        switch ($name) {
            case static::VAR_NAME:
                if (\is_string($name)) {
                    $this->name = $value;
                } else {
                    throw new \TeaBreak\Exception\ConfigurationException('invalid $name');
                }
                break;

            case static::VAR_VALUE:
                $this->_setValue($value);
                break;

            case static::VAR_IS_REQUIRED:
                if (\is_bool($value)) {
                    $this->is_required = $value;
                } else {
                    throw new \TeaBreak\Exception\ConfigurationException('invalid $is_requierd.');
                }
                break;

            default:
                $this->_customSetter($name, $value);
        }
    }

    /**
     * その他特殊なパラメータがある場合用のsetter
     * @param string $name
     * @param mixed $value
     * @throws \TeaBreak\Exception\ConfigurationException
     */
    function _customSetter($name, $value)
    {
        throw new \TeaBreak\Exception\ConfigurationException($name . ' is not object paramater.');
    }

    /**
     * 値(value)を設定する
     * @param mixed $value
     */
    public function _setValue($value)
    {
        if ($this->isValid($value)) {
            $this->value = $value;
        } elseif ($this->is_required) {
            throw new \TeaBreak\Exception\ConfigurationException;
        }
    }

    /**
     * 値の妥当性チェック
     * @param mixed $value
     */
    abstract public function isValid($value);
}
