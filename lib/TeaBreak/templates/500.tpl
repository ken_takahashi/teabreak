<html>
    <head>
        <title>Internal Server Error</title>
    </head>
    <body>
        <h1>Internal Server Error</h1>
        {if $message}<p>{$message|escape}</p>
        {else}<p>Please contact web server administrator. </p>
        {/if}
        {if $stack_trace}<pre><code>{$stack_trace|escape}</code></pre>
        {/if}
        {if $mailaddr}<p><address><a href="mailto:{$mailaddr|escape}">{$mailaddr|escape}</a></address></p>
        {/if}
    </body>
</html>