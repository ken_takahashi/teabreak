<html>
    <head>
        <title>File Not Found</title>
    </head>
    <body>
        <h1>File Not Found</h1>
        {if $message}<p>{$message|escape}</p>
        {else}<p>The requeseted URL {$smarty.server.REQUEST_URI|escape} was not found on this server</p>
        {/if}
        {if $stack_trace}<pre><code>{$stack_trace|escape}</code></pre>
        {/if}
        {if $mailaddr}<p><address><a href="mailto:{$mailaddr|escape}">{$mailaddr|escape}</a></address></p>
        {/if}
    </body>
</html>