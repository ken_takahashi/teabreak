<?php

namespace TeaBreak\Database;

/**
 * DB接続をSingleton化するためにcloneできないようにしたPDOクラス
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class DisableClonePDO extends \PDO
{

    /**
     * インスタンスのクローンを作成する
     * 
     * Singleton化のため常に例外を返す
     * @throws \TeaBreak\Exception\RuntimeException
     */
    public function __clone()
    {
        throw new \TeaBreak\Exception\RuntimeException('Can not clone this object.');
    }

}

/**
 * Database の接続を管理するクラス
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class DatabaseHandler
{

    /**
     * インスタンス保持用変数
     * @access protected
     * @var \TeaBreak\Database\DatabaseHandler 
     */
    protected static $_instance = array();

    /**
     * インスタンス取得用クラスメソッド
     * @access public 
     * @param \TeaBreak\Database\DbConfiguration $conf
     * @return \TeaBreak\Database\DisableClonePDO
     */
    public static function getInstance(\TeaBreak\Configuration\AbstractDbConfiguration $conf)
    {
        if (isset(static::$_instance[$conf->name]) === FALSE) {
            new static($conf);
        }

        return static::$_instance[$conf->name];
    }

    /**
     * コンストラクタ
     * @access protected
     * @param \TeaBreak\Configuration\AbstractDbConfiguration $conf
     */
    protected function __construct(\TeaBreak\Configuration\AbstractDbConfiguration $conf)
    {
        $dbh = new \PDO($conf->dsn, $conf->username, $conf->password, $conf->options);
        $dbh->setAttribute(\PDO::ATTR_EMULATE_PREPARES, FALSE);
        $dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        static::$_instance[$conf->name] = &$dbh;
    }

}
