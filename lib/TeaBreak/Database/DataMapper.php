<?php

namespace TeaBreak\Database;

use PDO;
use PDOStatement;

/**
 * データベースのスキーマとPHPクラスをマッピングする基底クラス
 *
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
abstract class DataMapper
{

    /**
     * PDOインスタンスを保持する変数
     * @var \PDO
     */
    protected $_pdo;

    /**
     * コンストラクタ
     * @access public
     * @param PDO $pdo PDOデータベースハンドラ
     */
    public function __construct(PDO $pdo)
    {
        $this->_pdo = $pdo;
    }

    /**
     * ステートメントにクラスを適用する
     * @param PDOStatement $stmt
     * @return \PDOStatement
     */
    protected function _Ormapping(PDOStatement $stmt)
    {
        $stmt->setFetchMode(PDO::FETCH_CLASS, static::MODEL_CLASS);
        return $stmt;
    }

    /**
     * 全データを取得する
     * @return PDOStatment
     */
    public function find_all()
    {
        $model = static::MODEL_CLASS;
        $table = $model::TABLE_NAME;
        $stmt = $this->_pdo->query(\sprintf('SELECT * FROM %s', $table));
        return $this->_Ormapping($stmt);
    }

    /**
     * 検索
     * @access public
     * @abstract
     */
    abstract public function find(array $from);

    /**
     * 保存
     * @access public
     * @param $entity
     * @abstract
     */
    abstract public function save($entity);

    /**
     * 削除
     * @access public
     * @abstract
     */
    abstract public function delete($entity);
}
