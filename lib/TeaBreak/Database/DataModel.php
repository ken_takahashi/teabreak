<?php

namespace TeaBreak\Database;

/**
 * データベースのスキーマ構成に相当するデータモデルの基底クラス
 *
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
abstract class DataModel
{

    /**
     * スキーマー定義の元
     * @access protected
     * @var array
     */
    protected static $_schema = array();

    /**
     * スキーマー定義のオブジェクト配列
     * @access protected
     * @var array
     */
    protected static $_schema_obj = array();

    /**
     * データ保持用配列
     * @access protected
     * @var array
     */
    protected $_data = array();

    /**
     * プロパティ参照メソッド
     * @access public
     * @param  string $prop プロパティ名
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function __get($prop)
    {
        if (isset($this->_data[$prop])) {
            return $this->_data[$prop];
        } elseif (isset(static::$_schema[$prop])) {
            return null;
        } else {
            throw new \InvalidArgumentException;
        }
    }

    /**
     * プロパティ存在確認メソッド
     * @access public
     * @param  string $prop プロパティ名
     * @return boolean
     */
    public function __isset($prop)
    {
        return isset($this->_data[$prop]);
    }

    /**
     * プロパティ更新メソッド
     * @access public
     * @param  string $prop プロパティ名
     * @param  mixed  $val  値
     * @throws \LogicException
     */
    public function __set($prop, $val)
    {
        if (!isset(static::$_schema[$prop])) {
            // そんなカラムはない
            throw new \LogicException('no such column.');
        }

        $schema = $this->_getSchemaObj($prop);

        $this->_data[$prop] = $schema->cast($val);
    }

    /**
     * スキーマオブジェクトを取得する
     *
     * クラスメンバの初期化にクラスが使えないので苦肉の策
     * @access protected
     * @param string $prop プロパティ名
     * @return \TeaBreak\Column\Column
     * @throws \InvalidArgumentException
     */
    protected function _getSchemaObj($prop)
    {
        $schema = array_key_exists($prop, static::$_schema_obj) ? static::$_schema_obj[$prop] : null;
        if (($schema instanceof \TeaBreak\Database\Column\Column) === false) {
            // スキーマオブジェクトが作成されていないので、オブジェクトを作成して格納する
            $schema_opt = static::$_schema[$prop];
            if (\array_key_exists('type', $schema_opt)) {
                $columnCls = '\\TeaBreak\\Database\\Column\\' . $schema_opt['type'];
                $schema = new $columnCls($schema_opt);
                static::$_schema_obj[$prop] = $schema;
            } else {
                // スキーマ定義で型定義が行われていない or 定義がおかしい
                throw new \LogicException('column definition is invalid.');
            }
        }

        return $schema;
    }

    /**
     * プロパティの配列を返す
     * @access public
     * @return array
     */
    public function toArray()
    {
        $arr = $this->_data;

        // 配列の正規化
        foreach (\array_keys(static::$_schema) as $key) {
            if (\array_key_exists($key, $arr) === FALSE) {
                $arr[$key] = null;
            }
        }

        return $arr;
    }

    /**
     * プロパティに配列で値をセットする
     * @access public
     * @param  array $arr プロパティ名をキーにした連想配列
     * @return void
     * @throws \InvalidArgumentException
     */
    public function fromArray(array $arr)
    {
        foreach ($arr as $key => $val) {
            $this->__set($key, $val);
        }
    }

    /**
     * バリデータ
     * @access public
     * @return boolean
     */
    public function isValid()
    {
        foreach (\array_keys(static::$_schema) as $prop) {
            $schema = $this->_getSchemaObj($prop);
            if ($schema->isValid($this->_data[$prop]) === false) {
                return false;
            }
        }

        return true;
    }

}
