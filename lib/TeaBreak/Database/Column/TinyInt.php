<?php

namespace TeaBreak\Database\Column;

require_once 'Int.php';

/**
 * TINYINT型カラムを定義するクラス
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class TinyInt extends Int
{

    /**
     * 符号付きの最小値
     * @var int
     */
    const SINGNED_MIN = -128;

    /**
     * 符号付きの最大値
     * @var int
     */
    const SINGNED_MAX = 127;

    /**
     * 符号なしの最大値
     * @var int
     */
    const UNSIGNED_MAX = 255;

    /**
     * 表示桁数のデフォルト値
     * @var
     */
    const DEFAULT_LENGTH = 4;

}
