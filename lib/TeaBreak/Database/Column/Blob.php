<?php

namespace TeaBreak\Database\Column;

require_once 'Text.php';

/**
 * BLOB型のカラムを定義するクラス
 * ※TEXT型のエイリアス
 */
\class_alias('Text', 'Blob');
