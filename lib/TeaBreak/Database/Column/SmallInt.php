<?php

namespace TeaBreak\Database\Column;

require_once 'Int.php';

/**
 * SMALLINT型カラムを定義するクラス
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class SmallInt extends Int
{

    /**
     * 符号付きの最小値
     */
    const SINGNED_MIN = -32768;

    /**
     * 符号付きの最大値
     */
    const SINGNED_MAX = 32767;

    /**
     * 符号なしの最大値
     */
    const UNSIGNED_MAX = 65535;

    /**
     * 表示桁数のデフォルト値
     */
    const DEFAULT_LENGTH = 6;

}
