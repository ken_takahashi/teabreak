<?php

namespace TeaBreak\Database\Column;

require_once 'Column.php';
require_once 'String.php';

/**
 * CHAR型のカラムを定義するクラス
 * @author Ken-ichiro Takahashi <ken_takahashi@yahoo.co.jp>
 */
class Char extends Column implements String
{

    /**
     * カラムのデータ型
     * @var string
     */
    const DATATYPE = 'string';

    /**
     * 文字列長の最大値（文字数）
     * @var int
     */
    const LANGTH_MAX = 255;

    /**
     * バイナリーフラグのデフォルト値
     * @var boolean
     */
    const DEFAULT_BINARY = \FALSE;

    /**
     * length のデフォルト値
     * @var int
     */
    const DEFAULT_LENGTH = 1;

    /**
     * バイナリーフラグ
     * @access protected
     * @var boolean
     */
    protected $binary;

    /**
     * 文字列長
     * @access protected
     * @var int
     */
    protected $length;

    /**
     * バイナリフラグを設定する
     * @access public
     * @param  boolean $var
     * @throws \InvalidArgumentException
     */
    public function setBinary($var)
    {
        if (is_bool($var)) {
            $this->binary = $var;
        } else {
            throw new \InvalidArgumentException;
        }
    }

    /**
     * バイナリフラグの状態を取得する
     * @access public
     * @return boolean
     */
    public function getBinary()
    {
        if (\is_null($this->binary)) {
            $this->binary = static::DEFAULT_BINARY;
        }

        return $this->binary;
    }

    /**
     * 文字列長を設定する
     * @access public
     * @param  int $var 文字数
     * @throws \InvalidArgumentException
     */
    public function setLength($var)
    {
        if (\is_int($var) && static::LANGTH_MAX >= $var) {
            // 数値かつ文字列長の最大値以下なら値を設定する
            $this->length = $var;
        } else {
            throw new \InvalidArgumentException;
        }
    }

    /**
     * 文字列長の状態を取得する
     * @access public
     * @return int
     */
    public function getLength()
    {
        if (\is_null($this->length)) {
            $this->length = static::DEFAULT_LENGTH;
        }

        return $this->length;
    }

    /**
     * カラムの型に合う値にキャストする
     * @access public
     * @param  mixed $var
     * @return string
     * @throws \InvalidArgumentException
     */
    public function cast($var)
    {
        if ($this->notNull === \FALSE && \is_null($var)) {
            // Not NULL 制約ではない場合はNULLはOK
            return \NULL;
        } elseif (is_scalar($var) === \FALSE) {
            // スカラー以外はNG
            throw new \InvalidArgumentException;
        } else {
            $var = (string) $var;
        }

        if ($this->length > \mb_strlen($var)) {
            // 設定されている文字列長以上はNG
            throw new \InvalidArgumentException;
        }

        return $var;
    }

}
