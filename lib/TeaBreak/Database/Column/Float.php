<?php

namespace TeaBreak\Database\Column;

require_once 'Column.php';
require_once 'Number.php';

/**
 * 単精度浮動小数点型カラムを定義するクラス
 * @author Ken-ichiro Takahashi <ken_takahashi@yahoo.co.jp>
 */
class Float extends Column implements Number
{

    /**
     * カラムのデータ型
     * @var string
     */
    const DATATYPE = 'float';

    /**
     * 型のとりうる最小値
     * @var float
     */
    const MINIMUM_VALUE = -3.402823466E+38;

    /**
     * 型のとりうる最大値
     * @var float
     */
    const MAXIMUM_VALUE = 3.402823466E+38;

    /**
     * 桁数の最大値
     * @var int
     */
    const MAXIMUM_DIGIT = 30;

    /**
     * デフォルトの桁数
     * @var int|null
     */
    const DEFAULT_DIGIT = \NULL;

    /**
     * 最大の小数点以下の桁数
     * @var int
     */
    const MAXIMUM_DECIMALS = 30;

    /**
     * デフォルトの小数点以下の桁数
     * @var int|null
     */
    const DEFAULT_DECIMALS = \NULL;

    /**
     * unsigned のデフォルト値
     * @var boolean
     */
    const DEFAULT_UNSINGNED = \FALSE;

    /**
     * zerofill のデフォルト値
     * @var boolean
     */
    const DEFAULT_ZEROFILL = \FALSE;

    /**
     * 符号なしフラグ
     * @access protected
     * @var boolean
     */
    protected $unsigned;

    /**
     * ゼロ埋めフラグ
     * @access protected
     * @var boolean
     */
    protected $zeroFill;

    /**
     * 総桁数桁数
     * @access protected
     * @var int
     */
    protected $totalDigit;

    /**
     * 小数点以下の桁数
     * @access protected
     * @var int
     */
    protected $decimalPlaces;

    /**
     * 符号なしフラグの設定する
     * @access public
     * @param  boolean $var
     * @throws \InvalidArgumentException
     */
    public function setUnsigned($var)
    {
        if (\is_bool($var)) {
            $this->unsigned = $var;
        } else {
            throw new \InvalidArgumentException;
        }
    }

    /**
     * 符号なしフラグの状態を取得する
     * @access public
     * @return boolean
     */
    public function getUnsigned()
    {
        if (\is_null($this->unsigned)) {
            $this->unsigned = static::DEFAULT_UNSINGNED;
        }

        return $this->unsigned;
    }

    /**
     * 0埋めフラグを設定する
     *
     * ※0埋め自体はDB任せなのでテーブル作成時以外にプログラム側で意識することはない？
     * @access public
     * @param  boolean $var
     * @throws \InvalidArgumentException
     */
    public function setZeroFill($var)
    {
        if (\is_bool($var)) {
            $this->zeroFill = $var;
        } else {
            throw new \InvalidArgumentException;
        }
    }

    /**
     * 0埋めフラグの状態を取得する
     * @access public
     * @return boolean
     */
    public function getZeroFill()
    {
        if (\is_null($this->zeroFill)) {
            $this->zeroFill = static::DEFAULT_ZEROFILL;
        }

        return $this->zeroFill;
    }

    /**
     * 総桁数の設定をする
     * @access public
     * @param  boolean $var
     * @throws \InvalidArgumentException
     */
    public function setTotalDigit($var)
    {
        if (\is_int($var) && $var > 0) {
            $this->totalDigit = $var;
        } else {
            throw new \InvalidArgumentException;
        }
    }

    /**
     * 総桁数の状態をを取得する
     * @access public
     * @return int
     */
    public function getTotalDigit()
    {
        if (\is_null($this->totalDigit)) {
            $this->length = static::DEFAULT_DIGIT;
        }

        return $this->length;
    }

    /**
     * 小数点以下の桁数の設定をする
     * @access public
     * @param  int $var
     * @throws \InvalidArgumentException
     */
    public function setDecimalPlaces($var)
    {
        if (\is_int($var) && $var > 0) {
            $this->decimalPlaces = $var;
        } else {
            throw new \InvalidArgumentException;
        }
    }

    /**
     * 小数点以下の桁数の状態を取得する
     * @access public
     * @return boolean
     */
    public function getDecimalPlaces()
    {
        if (\is_null($this->decimalPlaces)) {
            $this->decimalPlaces = static::DEFAULT_DECIMALS;
        }

        return $this->decimalPlaces;
    }

    /**
     * カラムの型に合う値にキャストする
     * @access public
     * @param  mixed $var 判定する値
     * @return float
     * @throws \InvalidArgumentException
     */
    public function cast($var)
    {
        if ($this->notNull === \FALSE && \is_null($var)) {
            // Not NULL 制約がない場合、NULLはOK
            return \NULL;
        } elseif (is_scalar($var) === \FALSE) {
            // スカラー以外はNG
            throw new \InvalidArgumentException;
        } else {
            $var = (int) $var;
        }

        if ($this->unsigned) {
            // 符号なしの時は 0 ～ static::UNSIGNED_MAX 以外の数値はNG
            if ($var < 0 || static::UNSIGNED_MAX < $var) {
                throw new \InvalidArgumentException;
            }
        } else {
            // 符号付きの時は static::SIGNED_MIN ～ static::SIGNED_MAS 以外の数値はNG
            if (static::SINGNED_MIN > $var || static::SINGNED_MAX < $var) {
                throw new \InvalidArgumentException;
            }
        }

        return $var;
    }

}
