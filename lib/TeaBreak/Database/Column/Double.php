<?php

namespace TeaBreak\Database\Column;

require_once 'Float.php';

/**
 * 倍精度浮動小数点型カラムを定義するクラス
 * @author Ken-ichiro Takahashi <ken_takahashi@yahoo.co.jp>
 */
class Double extends Float
{

    /**
     * カラムのデータ型
     * @var string
     */
    const DATATYPE = 'double';

    /**
     * 型のとりうる最小値
     * @var double
     */
    const MINIMUM_VALUE = -1.7976931348623157E+308;

    /**
     * 型のとりうる最大値
     * @var double
     */
    const MAXIMUM_VALUE = 1.7976931348623157E+308;

}
