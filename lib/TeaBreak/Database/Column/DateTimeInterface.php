<?php

namespace TeaBreak\Database\Column;

/**
 * 日付・時刻型のインターフェース
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
interface DateTimeInterface
{

    /**
     * デフォルト値を現在日時にするフラグの設定
     * @param boolean $var
     */
    public function setDefaultCurrentTimestamp($var);

    /**
     * デフォルト値を現在日時にするフラグの状態を取得する
     * @return boolean
     */
    public function getDefaultCurrentTimestamp();
}
