<?php

namespace TeaBreak\Database\Column;

require_once 'Int.php';

/**
 * BIGINT型カラムを定義するクラス
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class BigInt extends Int
{

    /**
     * 符号付きの最小値
     */
    const SINGNED_MIN = -9223372036854775808;

    /**
     * 符号付きの最大値
     */
    const SINGNED_MAX = 9223372036854775807;

    /**
     * 符号なしの最大値
     */
    const UNSIGNED_MAX = 18446744073709551615;

    /**
     * 表示桁数のデフォルト値
     */
    const DEFAULT_LENGTH = 20;

}
