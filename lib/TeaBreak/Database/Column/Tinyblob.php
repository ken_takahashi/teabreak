<?php

namespace TeaBreak\Database\Column;

require_once 'Text.php';

/**
 * TINYBLOB型カラムを定義するクラス
 * @author Ken-ichiro Takahashi <ken_takahashi@yahoo.co.jp>
 */
class Tinyblob extends Text
{

    /**
     * 文字列長の最大値（バイト数）
     * @var int
     */
    const LENGTH_MAX = 256;

}
