<?php

namespace TeaBreak\Database\Column;

require_once 'Int.php';

/**
 * MEDIUMINT型カラムを定義するクラス
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class MediumInt extends Int
{

    /**
     * 符号付きの最小値
     * @var int
     */
    const SINGNED_MIN = -8388608;

    /**
     * 符号付きの最大値
     * @var int
     */
    const SINGNED_MAX = 8388607;

    /**
     * 符号なしの最大値
     * @var int
     */
    const UNSIGNED_MAX = 16777215;

    /**
     * 表示桁数のデフォルト値
     * @var int
     */
    const DEFAULT_LENGTH = 8;

}
