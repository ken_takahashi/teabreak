<?php

namespace TeaBreak\Database\Column;

/**
 * 数値型のインターフェイス
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
interface Number
{

    /**
     * 符号の有り無しフラグを設定する
     * @access public
     * @param boolean $var
     */
    public function setUnsigned($var);

    /**
     * 符号の有り無しフラグの状態を取得する
     * @access public
     * @return boolean
     */
    public function getUnsigned();

    /**
     * zerofill フラグを設定する
     * @access public
     * @param boolean $var
     */
    public function setZerofill($var);

    /**
     * zerofill フラグの状態を取得する
     * @access public
     * @return boolean
     */
    public function getZerofill();
}
