<?php

namespace TeaBreak\Database\Column;

require_once 'Char.php';

/**
 * TEXT型のカラムを定義するクラス
 * @author Ken-ichiro Takahashi <ken_takahashi@yahoo.co.jp>
 */
class Text extends Char
{

    /**
     * 文字列長の最大値（バイト数）
     * @var int
     */
    const LANGTH_MAX = 65536;

    /**
     * カラムの型に合う値にキャストする
     * @access public
     * @param  mixed $var
     * @return string
     * @throws \InvalidArgumentException
     */
    public function cast($var)
    {
        if ($this->notNull === \FALSE && \is_null($var)) {
            // Not NULL 制約ではない場合はNULLはOK
            return \NULL;
        } elseif (\is_scalar($var) === \FALSE) {
            // スカラー以外はNG
            throw new \InvalidArgumentException;
        } else {
            $var = (string) $var;
        }

        if ($this->length < \strlen($var)) {
            // 設定されている文字列長以上はNG
            throw new \InvalidArgumentException;
        }

        return $var;
    }

}
