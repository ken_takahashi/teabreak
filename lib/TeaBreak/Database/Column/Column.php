<?php

namespace TeaBreak\Database\Column;

/**
 * データモデルの型を定義する基底クラス
 * @author Ken-ichiro Takahashi <ken_takahashi@yahoo.co.jp>
 * @abstract
 */
abstract class Column
{

    /**
     * notNUll のデフォルト値
     * @var boolean
     */
    const DEFAULT_NOTNULL = \FALSE;

    /**
     * unique のデフォルト値
     * @var boolean
     */
    const DEFAULT_UNIQUE = \FALSE;

    /**
     * デフォルト値
     * @access protected
     * @var mixed
     */
    protected $defaultValue;

    /**
     * Not NULL制約
     * @access protected
     * @var boolean
     */
    protected $notNull;

    /**
     * ユニーク制約
     * @access protedted
     * @var boolean
     */
    protected $unique;

    /**
     * コンストラクタ
     * @access public
     * @param  array $args
     * @throws \InvalidArgumentException
     */
    public function __construct(array $args)
    {
        foreach ($args as $key => $val) {
            if ($key === 'type') {
                continue;
            }

            try {
                $setMethod = 'set' . \ucfirst($key);
                $this->$setMethod($val);
            } catch (\InvalidArgumentException $e) {
                throw $e;
            } catch (\Exception $e) {
                throw new \InvalidArgumentException($e->getMessage());
            }
        }
    }

    /**
     * 未定義メソッドが呼ばれた時の処理
     *
     * 素のままのPHPでは、未定義メソッドが呼ばれると Fatal Error になってしまう。
     * それでは都合がわるいので、未定義の場合には BadMethodCallException 例外を
     * スローするようにしておく。
     * @access public
     * @param string $method
     * @param array $arguments
     * @throws \BadMethodCallException
     */
    public function __call($method, $arguments)
    {
        throw new \BadMethodCallException(sprintf('%s is undefined method.', $method));
    }

    /**
     * デフォルト値を設定する
     * @access public
     * @param  int $var
     * @throws \InvalidArgumentException
     */
    public function setDefaultValue($var)
    {
        $this->defaultValue = $this->cast($var);
    }

    /**
     * デフォルト値を取得する
     * @access public
     * @return mixed
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * Not NULL 制約を設定する
     * @access public
     * @param  boolean $var
     * @throws \InvalidArgumentException
     */
    public function setNotNull($var)
    {
        if (\is_bool($var)) {
            $this->notNull = $var;
        } else {
            throw new \InvalidArgumentException;
        }
    }

    /**
     * Not NULL 制約の状態を取得する
     * @access public
     * @return boolean
     */
    public function getNotNull()
    {
        if (\is_null($this->notNull)) {
            $this->notNull = static::DEFAULT_NOTNULL;
        }

        return $this->notNull;
    }

    /**
     * unique 制約を設定する
     * @access public
     * @param  boolean $var
     * @throws \InvalidArgumentException
     */
    public function setUnique($var)
    {
        if (\is_bool($var)) {
            $this->unique = $var;
        } else {
            throw new \InvalidArgumentException;
        }
    }

    /**
     * unique 制約の状態を取得する
     * @access public
     * @return boolean
     */
    public function getUnique()
    {
        if (\is_null($this->unique)) {
            $this->unique = static::DEFAULT_UNIQUE;
        }

        return $this->unique;
    }

    /**
     * カラムの値として妥当かどうかを検査する
     * @access public
     * @param  mixed $var 判定する値
     * @return boolean
     * @throws \TeaBreak\Database\Column\InvalidArgumentException
     */
    public function isValid($var)
    {
        try {
            // キャストしてみて例外が出なければOK
            $this->cast($var);
        } catch (\InvalidArgumentException $e) {
            return \FALSE;
        } catch (\Exception $e) {
            throw $e;
        }

        return \TRUE;
    }

    /**
     * カラムの型に合う値にキャストする
     * @param mixed $var
     * @abstract
     */
    abstract public function cast($var);
}
