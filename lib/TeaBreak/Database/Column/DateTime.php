<?php

namespace TeaBreak\Database\Column;

require_once 'Column.php';
require_once 'DateTimeInterface.php';

/**
 * 日時型カラムを定義するクラス
 * @author Ken-ichiro Takahashi <ken_takahashi@yahoo.co.jp>
 */
class DateTime extends Column implements DateTimeInterface
{

    /**
     * カラムのデータ型
     * @var string
     */
    const DATATYPE = 'DateTime';

    /**
     * 型のとりうる最小値
     * @var string
     */
    const MINIMUM_VALUE = '1000-01-01 00:00:00';

    /**
     * 型のとりうる最大値
     * @var string
     */
    const MAXIMUM_VALUE = '9999-12-31 23:59:59';

    /**
     * ゼロ値
     * @var string
     */
    const ZERO_VALUE = '0000-00-00 00:00:00';

    /**
     * defaultCurrentTimestamp のデフォルト値
     * @var boolean
     */
    const DEFAULT_DEFAULT_CURRENT_TIMESTAMP = \FALSE;

    /**
     * デフォルトで current_timestamp を使うフラグ
     * @access protected
     * @var boolean
     */
    protected $defaultCurrentTimestamp;

    /**
     * タイムゾーンの設定
     * @access protected
     * @var \DateTimeZone
     */
    protected $timeZone;

    /**
     * defaultCurrentTimestamp を設定する
     * @access public
     * @param boolean
     * @throws \InvalidArgumentException
     */
    public function setDefaultCurrentTimestamp($var)
    {
        if (\is_bool($var)) {
            $this->defaultCurrentTimestamp = $var;
        } else {
            throw new \InvalidArgumentException;
        }
    }

    /**
     * defaultCurrentTimestamp の状態を取得する
     * @access public
     * @return boolean
     */
    public function getDefaultCurrentTimestamp()
    {
        if (\is_null($this->defaultCurrentTimestamp)) {
            $this->defaultCurrentTimestamp = static::DEFAULT_DEFAULT_CURRENT_TIMESTAMP;
        }

        return $this->defaultCurrentTimestamp;
    }

    /**
     * timeZone を設定する
     * @access public
     * @param \DateTimeZone
     */
    public function setTimeZone(\DateTimeZone $timeZone)
    {
        $this->timeZone = $timeZone;
    }

    /**
     * timeZone を取得する
     * @access public
     * @return \DateTimeZone
     */
    public function getTimeZone()
    {
        // 設定されていなかったらPHPのデフォルトタイムゾーンを使う
        if (\is_null($this->timeZone)) {
            $this->timeZone = new \DateTimeZone(date_default_timezone_get());
        }

        return $this->timeZone;
    }

    /**
     * カラムの型に合う値にキャストする
     * @access public
     * @param  mixed $var
     * @return \DateTime
     * @throws \InvalidArgumentException
     */
    public function cast($var)
    {
        $date = \NULL;
        if ($this->notNull === \FALSE && \is_null($var)) {
            // Not NULL 制約がない場合、NULLはOK
            return \NULL;
        } elseif ($this->defaultCurrentTimestamp === \TRUE && \is_null($var)) {
            // defaultCurrentTimestamp が指定されている場合も NULL はOK
            return \NULL;
        } elseif (\is_int($var) || (\is_scalar($var) && $var === \strval(\intval($var)))) {
            // Unix タイムスタンプとしてパースしてみる
            try {
                $date = new \DateTime("@$var");
            } catch (\Exception $ex) {
                throw new \InvalidArgumentException($ex->getMessage(), $ex->getCode, $ex);
            }
        } elseif (\is_string($var)) {
            // 日時文字列としてパースしてみる
            try {
                $date = new \DateTime($var);
            } catch (\Exception $ex) {
                throw new \InvalidArgumentException($ex->getMessage(), $ex->getCode(), $ex);
            }
        } else {
            // それ以外はNG
            throw new \InvalidArgumentException;
        }

        // とりうる日付の範囲内かどうかチェック
        if ($date instanceof \DateTime && new \DateTime(static::MINIMUM_VALUE) <= $date && new \DateTime(static::MAXIMUM_VALUE) >= $date) {
            $date->setTimeZone($this->getTimeZone());
            return $date;
        } else {
            throw new \InvalidArgumentException;
        }
    }

}
