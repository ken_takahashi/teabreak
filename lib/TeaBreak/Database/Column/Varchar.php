<?php

namespace TeaBreak\Database\Column;

require_once 'Char.php';

/**
 * varchar型カラムを定義するクラス
 * @author Ken-ichiro Takahashi <ken_takahashi@yahoo.co.jp>
 */
class Varchar extends Char
{

    /**
     * 文字列長の最大値（文字数）
     * @var int
     */
    const LENGTH_MAX = 65535;

}
