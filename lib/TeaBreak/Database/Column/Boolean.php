<?php

namespace TeaBreak\Database\Column;

require_once 'Column.php';

/**
 * Boolean型カラムを定義するクラス
 * @author Ken-ichiro Takahashi <ken_takahashi@yahoo.co.jp>
 */
class Boolean extends Column
{

    /**
     * カラムのデータ型
     * @var string
     */
    const DATATYPE = 'boolean';

    /**
     * カラムの値として妥当かどうかを検査する
     * @access public
     * @param  int $var
     * @return boolean
     */
    public function isValid($var)
    {
        // ブーリアンは何が来ても基本OK
        return \TRUE;
    }

    /**
     * カラムの型に合う値にキャストする
     * @access public
     * @param  mixed $var
     * @return boolean
     */
    public function cast($var)
    {
        if ($this->notNull === \FALSE && \is_null($var)) {
            // Not NULL 制約ではない場合はNULLはOK
            return \NULL;
        } else {
            return (boolean) $var;
        }
    }

}
