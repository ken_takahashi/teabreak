<?php

namespace TeaBreak\Database\Column;

require_once 'Column.php';
require_once 'Number.php';

/**
 * 整数型カラムを定義するクラス
 * @author Ken-ichiro Takahashi <ken_takahashi@yahoo.co.jp>
 */
class Int extends Column implements Number
{

    /**
     * カラムのデータ型
     * @var string
     */
    const DATATYPE = 'int';

    /**
     * 符号付きの最小値
     * @var int
     */
    const SINGNED_MIN = -2147483648;

    /**
     * 符号付きの最大値
     * @var int
     */
    const SINGNED_MAX = 2147483647;

    /**
     * 符号なしの最大値
     * @var int
     */
    const UNSIGNED_MAX = 4294967295;

    /**
     * 表示桁数のデフォルト値
     * @var int
     */
    const DEFAULT_LENGTH = 11;

    /**
     * 符号なしフラグ
     * @access protected
     * @var boolean
     */
    protected $unsigned;

    /**
     * ゼロ埋めフラグ
     * @access protected
     * @var boolean
     */
    protected $zeroFill;

    /**
     * 表示桁数
     * @access protected
     * @var int
     */
    protected $length;

    /**
     * 符号なしフラグの設定する
     * @access public
     * @param  boolean $var
     * @throws \InvalidArgumentException
     */
    public function setUnsigned($var)
    {
        if (\is_bool($var)) {
            $this->unsigned = $var;
        } else {
            throw new \InvalidArgumentException;
        }
    }

    /**
     * 符号なしフラグの状態を取得する
     * @access public
     * @return boolean
     */
    public function getUnsigned()
    {
        if (\is_null($this->unsigned)) {
            $this->unsigned = static::DEFAULT_UNSINGNED;
        }

        return $this->unsigned;
    }

    /**
     * 0埋めフラグを設定する
     *
     * ※0埋め自体はDB任せなのでテーブル作成時以外にプログラム側で意識することはない？
     * @access public
     * @param  boolean $var
     * @throws \InvalidArgumentException
     */
    public function setZeroFill($var)
    {
        if (\is_bool($var)) {
            $this->zeroFill = $var;
        } else {
            throw new \InvalidArgumentException;
        }
    }

    /**
     * 0埋めフラグの状態を取得する
     * @access public
     * @return boolean
     */
    public function getZeroFill()
    {
        if (\is_null($this->zeroFill)) {
            $this->zeroFill = static::DEFAULT_ZEROFILL;
        }

        return $this->zeroFill;
    }

    /**
     * 表示桁数を設定する
     *
     * ※DBのテーブル作成時に表示桁数設定するために使用されるだけで
     * 　プログラム上ではこの値が使用されることは殆ど無い？
     * @access public
     * @param  boolean $var
     * @throws \InvalidArgumentException
     */
    public function setLength($var)
    {
        if (\is_int($var) && $var > 0) {
            $this->length = $var;
        } else {
            throw new \InvalidArgumentException;
        }
    }

    /**
     * 表示桁数の状態をを取得する
     * @access public
     * @return int
     */
    public function getLength()
    {
        if (\is_null($this->length)) {
            $this->length = static::DEFAULT_LENGTH;
        }

        return $this->length;
    }

    /**
     * カラムの型に合う値にキャストする
     * @access public
     * @param  mixed $var 判定する値
     * @return int
     * @throws \InvalidArgumentException
     */
    public function cast($var)
    {
        if ($this->notNull === \FALSE && \is_null($var)) {
            // Not NULL 制約がない場合、NULLはOK
            return \NULL;
        } elseif (\is_scalar($var) === \FALSE) {
            // スカラー以外はNG
            throw new \InvalidArgumentException('is not scalar.');
        } else {
            $var = (int) $var;
        }

        if ($this->unsigned) {
            // 符号なしの時は 0 ～ static::UNSIGNED_MAX 以外の数値はNG
            if ($var < 0 || static::UNSIGNED_MAX < $var) {
                throw new \InvalidArgumentException('out of range.');
            }
        } else {
            // 符号付きの時は static::SIGNED_MIN ～ static::SIGNED_MAS 以外の数値はNG
            if (static::SINGNED_MIN > $var || static::SINGNED_MAX < $var) {
                throw new \InvalidArgumentException('is singned.');
            }
        }

        return $var;
    }

}
