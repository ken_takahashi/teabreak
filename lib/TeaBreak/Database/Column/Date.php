<?php

namespace TeaBreak\Database\Column;

require_once 'Column.php';
require_once 'DateTimeInterface.php';

/**
 * 日時型カラムを定義するクラス
 * @author Ken-ichiro Takahashi <ken_takahashi@yahoo.co.jp>
 */
class Date extends DateTime
{

    /**
     * カラムのデータ型
     */
    const DATATYPE = 'DateTime';

    /**
     * 型のとりうる最小値
     */
    const MINIMUM_VALUE = '1000-01-01';

    /**
     * 型のとりうる最大値
     */
    const MAXIMUM_VALUE = '9999-12-31';

    /**
     * ゼロ値
     */
    const ZERO_VALUE = '0000-00-00';

    /**
     * defaultCurrentTimestamp のデフォルト値
     */
    const DEFAULT_DEFAULT_CURRENT_TIMESTAMP = \FALSE;

    /**
     * デフォルトで current_timestamp を使うフラグ
     * @access protected
     * @var boolean
     */
    protected $defaultCurrentTimestamp;

    /**
     * defaultCurrentTimestamp を設定する
     * @access public
     * @param boolean
     * @throws \InvalidArgumentException
     */
    public function setDefaultCurrentTimestamp($var)
    {
        if (\is_bool($var)) {
            $this->defaultCurrentTimestamp = $var;
        } else {
            throw new \InvalidArgumentException;
        }
    }

    /**
     * defaultCurrentTimestamp の状態を取得する
     * @access public
     * @return boolean
     */
    public function getDefaultCurrentTimestamp()
    {
        if (\is_null($this->defaultCurrentTimestamp)) {
            $this->defaultCurrentTimestamp = static::DEFAULT_DEFAULT_CURRENT_TIMESTAMP;
        }

        return $this->defaultCurrentTimestamp;
    }

    /**
     * カラムの型に合う値にキャストする
     * @access public
     * @param  mixed $var
     * @return \DateTime
     * @throws \InvalidArgumentException
     */
    public function cast($var)
    {
        $date = \NULL;
        if ($this->notNull === \FALSE && \is_null($var)) {
            // Not NULL 制約がない場合、NULLはOK
            return \NULL;
        } elseif ($this->defaultCurrentTimestamp === \TRUE && \is_null($var)) {
            // defaultCurrentTimestamp が指定されている場合も NULL はOK
            return \NULL;
        } elseif (\is_int($var)) {
            // Unix タイムスタンプとしてパースしてみる
            $date = new \DateTime("@$var");
        } elseif (\is_string($var)) {
            // 日時文字列としてパースしてみる
            $date = new \DateTime($var);
        } else {
            // それ以外はNG
            throw new \InvalidArgumentException;
        }

        if ($date instanceof \DateTime && new \DateTime(static::MINIMUM_VALUE) <= $date && new \DateTime(static::MAXIMUM_VALUE) >= $date) {
            return $date;
        } else {
            throw new \InvalidArgumentException;
        }
    }

}
