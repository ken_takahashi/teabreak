<?php

namespace TeaBreak\Database\Column;

/**
 * 文字列型のインターフェース
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
interface String
{

    /**
     * binary フラグを設定する
     * @access public
     * @param boolean
     */
    public function setBinary($var);

    /**
     * binary フラグの状態を取得する
     * @access public
     * @return boolean
     */
    public function getBinary();

    /**
     * 文字列長を設定する
     * @access public
     * @param int
     */
    public function setLength($var);

    /**
     * 文字列長の状態を取得する
     * @access public
     * @return int
     */
    public function getLength();
}
