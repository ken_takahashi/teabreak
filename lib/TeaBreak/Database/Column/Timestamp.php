<?php

namespace TeaBreak\Database\Column;

require_once 'Column.php';
require_once 'DateTime.php';

/**
 * タイムスタンプ型カラムを定義するクラス
 * @author Ken-ichiro Takahashi <ken_takahashi@yahoo.co.jp>
 */
class Timestamp extends DateTime
{

    /**
     * カラムのPHPでのデータ型
     * @var string
     */
    const DATATYPE = 'DateTime';

    /**
     * 型のとりうる最小値
     * @var string
     */
    const MINIMUM_VALUE = '1970-01-01 00:00:00';

    /**
     * 型のとりうる最大値
     * @var string
     */
    const MAXIMUM_VALUE = '2106-12-31 23:59:59';

    /**
     * ゼロ値
     * @var string
     */
    const ZERO_VALUE = '0000-00-00 00:00:00';

    /**
     * UPDATE で current_timestamp を使うフラグ
     * @var boolean
     */
    protected $updateCurrentTimestamp;

    /**
     * updateCurrentTimestamp を設定する
     * @access public
     * @param boolean
     * @throws \InvalidArgumentException
     */
    public function setUpdateCurrentTimestamp($var)
    {
        if (\is_bool($var)) {
            $this->updateCurrentTimestamp = $var;
        } else {
            throw new \InvalidArgumentException;
        }
    }

    /**
     * updateCurrentTimestamp の状態を取得する
     * @access public
     * @return boolean
     */
    public function getUpdateCurrentTimestamp()
    {
        return $this->updateCurrentTimestamp;
    }

}
