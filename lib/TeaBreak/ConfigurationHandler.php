<?php

namespace TeaBreak;

/**
 * 設定ファイル取得クラス
 */
class ConfigurationHandler
{

    /**
     * インスタンス保持用変数
     * @var __CLASS__
     */
    protected static $_instance;

    /**
     * 設定ファイル
     */
    protected static $_confFile;

    /**
     * 設定の配列(raw data)
     * @var array
     */
    protected $_conf = array();

    /**
     * 設定の配列(object data)
     * @var array
     */
    protected $_container = array();

    /**
     * 設定ファイルの指定
     * 
     * @param string $confFile
     * @throws \TeaBreak\Exception\WarningException
     * @throws \TeaBreak\Exception\ConfigurationException
     */
    public static function setConfigurationFile($confFile)
    {
        if (isset(static::$_conf)) {
            throw new \TeaBreak\Exception\WarningException('Instance configured');
        } elseif (\realpath($confFile) === \FALSE) {
            throw new \TeaBreak\Exception\ConfigurationException($confFile . ' is not found.');
        }
        static::$_confFile = \realpath($confFile);
    }

    /**
     * インスタンス取得
     * @param  string $confFile
     * @return __CLASS__
     * @throws \TeaBreak\Exception\ConfigurationException
     */
    public static function getInstance()
    {
        if (\is_null(static::$_instance)) {
            static::$_instance = new static();
        }

        return static::$_instance;
    }

    /**
     * コンストラクタ
     * @param  string $confFile
     * @throws \TeaBreak\Exception\ConfigurationException
     */
    protected function __construct()
    {
        if (empty(static::$_confFile)) {
            throw new \TeaBreak\Exception\ConfigurationException('configuration file path undefined.');
        }

        $conf = null;
        if (\file_exists(static::$_confFile)) {
            if (\function_exists('\yaml_parse_file')) {
                $conf = \yaml_parse_file(static::$_confFile);
            } else {
                $conf = \Spyc::YAMLLoad(static::$_confFile);
            }
        } else {
            throw new \TeaBreak\Exception\ConfigurationException(static::$_confFile . ' is not found.');
        }

        $this->_conf = $conf;
        foreach (\array_keys($this->_conf) as $name) {
            $this->_createConfiguration($name);
        }
    }

    /**
     * インスタンスのクローンを作成する
     * 
     * Singleton化のため常に例外を返す
     * @throws \TeaBreak\Exception\RuntimeException
     */
    public function __clone()
    {
        throw new \TeaBreak\Exception\RuntimeException('Can not clone this object.');
    }

    /**
     * getter
     * @param  string $name
     * @return mixed
     */
    public function __get($name)
    {
        if (isset($this->_container[$name])) {
            return $this->_container[$name];
        } elseif (isset($this->config[$name])) {
            $this->_createConfiguration($name);

            return $this->_container[$name];
        } else {
            return null;
        }
    }

    /**
     * 設定クラスの作成
     * @param string $name
     */
    protected function _createConfiguration($name)
    {
        $className = '\\TeaBreak\\Configuration\\' . \ucfirst($name) . 'Configuration';
        $this->_container[$name] = new $className($name, $this->_conf[$name]);
    }

    /**
     * 設定項目の一覧
     * @return array
     */
    public function getConfNameList()
    {
        return \array_keys($this->_container);
    }

}
