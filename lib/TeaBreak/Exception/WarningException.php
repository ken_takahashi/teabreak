<?php

namespace TeaBreak\Exception;

/**
 * 警告の例外
 *
 * キャッチして適切な処理をすれば処理続行可能な例外でスローする
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class WarningException extends \TeaBreak\Exception\TBException
{
    
}
