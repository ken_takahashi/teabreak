<?php

namespace TeaBreak\Exception;

/**
 * PHP の エラーを例外に変換するクラス
 */
class PhpErrorException extends \ErrorException
{

    private static $exceptErrorLevel = \E_ERROR;
    private static $excludeFileList = array();
    private static $logger = 'error_log';

    /**
     * キャッチするエラーレベルを指定する
     * E_ERROR か E_WARNING のみ指定可能。
     * @param int $level
     * @return bool
     */
    public static function setExceptErrorLevel($level)
    {
        if ($level === E_ERROR || $level === E_WARNING) {
            static::$exceptErrorLevel = $level;
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * ロギング対象外のファイル（正規表現パターン指定）を一つ追加する
     * @param string $file
     */
    public static function addExcludeFile($file)
    {
        if (is_string($file)) {
            static::$excludeFileList[] = $file;
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * ロギング対象外のファイル（正規表現パターン指定）をリストで追加する
     * @param array $files
     */
    public static function addExcludeFiles($files)
    {
        if (is_array($files)) {
            array_merge(static::$excludeFileList, $files);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * ロガーの設定
     */
    public static function setLogger(callable $logger)
    {
        static::$logger = $logger;
    }

    /**
     * set_error_handler で呼ばれるメソッド
     * @param int $errno
     * @param string $errstr
     * @param string $errfile
     * @param int $errline
     * @return boolean
     * @throws self
     */
    public static function throwException($errno, $errstr, $errfile, $errline)
    {
        switch ($errno) {
            case \E_ERROR:
            case \E_PARSE:
            case \E_CORE_ERROR:
            case \E_COMPILE_ERROR:
                // 普通はここにこない
                call_user_func(static::$logger, $errstr);
            case \E_RECOVERABLE_ERROR:
            case \E_USER_ERROR:
                throw new self($errstr, $errno, $errno, $errfile, $errline);

            case \E_WARNING:
            case \E_CORE_WARNING:
            case \E_COMPILE_WARNING:
            case \E_USER_WARNING:
                if (static::$exceptErrorLevel === \E_WARNING) {
                    throw new self($errstr, $errno, $errno, $errfile, $errline);
                } else {
                    foreach (static::$excludeFileList as $pattern) {
                        if (\preg_match($pattern, $errfile) === 1) {
                            return;
                        }
                    }
                    call_user_func(static::$logger, $errstr);
                    return;
                }
                break;

            case \E_DEPRECATED:
            case \E_USER_DEPRECATED:
                call_user_func(static::$logger, $errstr);
            default:
                return;
        }
    }

}
