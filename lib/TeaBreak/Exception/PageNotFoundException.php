<?php

namespace TeaBreak\Exception;

/**
 * ページが見つからなかった場合の例外
 */
class PageNotFoundException extends RuntimeException
{
    
}
