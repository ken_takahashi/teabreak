<?php

namespace TeaBreak\Exception;

/**
 * 認証エラーの例外
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class AuthenticationException extends RuntimeException
{

    /**
     * 認証オブジェクト
     * @access protected
     * @var LibAuthHttp
     */
    protected $authHttp;

    /**
     * コンストラクタ
     * @access public
     * @param string $message
     * @param int $code
     * @param \Exception $previous
     * @param Object $authHttp
     */
    public function __construct($message = null, $code = 0, \Exception $previous = null, $authHttp = null)
    {
        if ($previous instanceof \Exception) {
            parent::__construct($message, $code, $previous);
        } else {
            parent::__construct($message, $code);
        }
        $this->authHttp = $authHttp;
    }

    /**
     * 認証オブジェクトを受け取る
     * @access public
     * @return LibAuthHttp
     */
    final public function getAuthHttp()
    {
        return $this->authHttp;
    }

}
