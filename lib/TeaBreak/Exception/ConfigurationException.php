<?php

namespace TeaBreak\Exception;

/**
 * 設定が不足している場合の例外
 */
class ConfigurationException extends RuntimeException
{
    
}
