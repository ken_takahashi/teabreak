<?php

namespace TeaBreak;

/**
 * アプリケーションコントローラ
 */
abstract class ApplicationController
{

    /**
     * 設定情報変数
     * @var \TeaBreak\ConfigurationHandler
     */
    protected $config;

    /**
     * リクエストハンドラ
     * @var \TeaBreak\RequestHandler
     */
    protected $request;

    /**
     * レスポンスハンドラ
     * @var \TeaBreak\ResponseHandler
     */
    protected $response;

    /**
     * ビューハンドラ
     * @var \TeaBreak\ViewHandler
     */
    protected $view;

    /**
     * コンストラクタ
     */
    public function __construct()
    {
        if (defined('SMARTY_DIR') === FALSE) {
            define('SMARTY_DIR', TEABREAK_HOME . DIRECTORY_SEPARATOR . 'vendor/smarty/smarty/distribution/libs/');
        }

        // Library Path
        $lib_path = realpath(TEABREAK_HOME . DIRECTORY_SEPARATOR . 'lib');
        // Vendor Path
        $vendor_path = realpath(TEABREAK_HOME . DIRECTORY_SEPARATOR . 'vendor');
        // Application Path
        $app_path = realpath(TEABREAK_HOME . DIRECTORY_SEPARATOR . 'src');

        // set include path
        $path_array = array($app_path, $lib_path, SMARTY_DIR, get_include_path());
        set_include_path(implode(PATH_SEPARATOR, $path_array));

        require_once realpath($vendor_path . DIRECTORY_SEPARATOR . 'autoload.php');

        require_once 'TeaBreak/Exception/PhpErrorException.php';
        set_error_handler(array('\\TeaBreak\\Exception\\PhpErrorException', 'throwException'));
        register_shutdown_function(array($this, 'showFatalErrorPage'));

        require_once 'TeaBreak/ClassLoader.php';
        require_once 'TeaBreak/Exception/TBException.php';

        $this->loader = new ClassLoader();
        $this->loader->registerDir(\TEABREAK_HOME . \DIRECTORY_SEPARATOR . 'lib');
        $this->_setConfigurationHandler();
        $this->_setRequestHandler();
        $this->_setViewHandler();
    }

    /**
     * configuration handlerを設定する
     */
    protected function _setConfigurationHandler()
    {
        \TeaBreak\ConfigurationHandler::setConfigurationFile(\TEABREAK_CONF);
        $this->config = ConfigurationHandler::getInstance();
    }

    /**
     * request handlerを設定する
     */
    protected function _setRequestHandler()
    {
        $this->request = RequestHandler::getInstance();
    }

    /**
     * response handlerを設定する
     */
    protected function _setResponseHandler()
    {
        $this->response = \TeaBreak\ResponseHandler::getInstance();
    }

    /**
     * viewhanlderを設定する
     */
    protected function _setViewHandler()
    {
        $this->view = new \TeaBreak\ViewHandler();
    }

    /**
     * メインルーチン
     */
    public function main()
    {
        try {
            $page_conf = $this->getPageConfiguration();
        } catch (\TeaBreak\Exception\PageNotFoundException $e) {
            $this->showErrorPage($e);
        }

        switch ($page_conf->type) {
            case 'page':
                $template = $page_conf->template;
                $this->view->display($template);
                break;

            case 'static':
            default:
                $uri = $this->request->getRequestUri();
                $filename = realpath(TEABREAK_HOME . DIRECTORY_SEPARATOR . 'htdocs' . DIRECTORY_SEPARATOR . preg_replace('/^(?:ht|f)tp(?:s|):\/\/[^\/]+(.*)$/', '$1', $uri));
                if ($filename) {
                    header('Content-type: ' . file($filename));
                    readfile($filename);
                    exit;
                } else {
                    $this->showErrorPage(new \TeaBreak\Exception\PageNotFoundException);
                }
                break;
        }
    }

    /**
     * ページ設定を取得
     * @return type
     * @throws TeaBreak\Exception\PageNotFoundException
     */
    public function getPageConfiguration()
    {
        $uri = parse_url($this->request->getRequestUri());
        $path = $uri['path'];
        try {
            return $this->config->routing->getPage($path);
        } catch (\TeaBreak\Exception\ConfigurationException $e) {
            throw new \TeaBreak\Exception\PageNotFoundException('Page Not Found.', 0, $e);
        }
    }

    /**
     * エラーページを表示
     * @param \Exception $e
     * @throws \Exception
     */
    public function showErrorPage(\Exception $e)
    {
        if (is_a($e, '\TeaBreak\Exception\PageNotFoundException')) {
            $this->view->showErrorPage($e);
        } else {
            throw $e;
        }
    }

    /**
     * fatal error reporting
     */
    public function showFatalErrorPage($param)
    {
        $trace = var_export($param, true);
        error_log($trace);
        
        header('HTTP', true, 500);

        require_once 'Smarty.php';
        $smarty = new \Smarty();
        $smarty->setTemplateDir(TEABREAK_HOME . DIRECTORY_SEPARATOR . 'templates');
        $tpl = $smarty->createTemplate('500.tpl');
        
        // パラメータのダンプを出力
        if(!empty($param)) {
            $tpl->assign('stack_trace', $trace);
        }
        
        // メールアドレスが取れれば出力
        if(defined($this->config) && defined($this->config->common->mailaddr)) {
            $tpl->assign('mailaddr', $this->config->common->mailaddr);
        }

        $tpl->display();
    }
}
