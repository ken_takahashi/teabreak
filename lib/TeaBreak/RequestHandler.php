<?php

namespace TeaBreak;

/**
 * HTTP リクエストを管理するクラス
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class RequestHandler
{

    /**
     * インスタンス保持用変数
     * @var __CLASS__
     */
    protected static $_instance;

    /**
     * コンストラクタ
     */
    private function __construct()
    {
        
    }

    /**
     * インスタンスを取得する
     * @return __CLASS__
     */
    public static function getInstance()
    {
        if (\is_null(static::$_instance)) {
            static::$_instance = new static();
        }

        return static::$_instance;
    }

    /**
     * REQUEST METHOD を取得する
     * @return string (POST|GET|PUT|...)
     */
    public function getRequestMethod()
    {
        return \filter_id(\INPUT_SERVER, 'REQUEST_METHOD');
    }

    /**
     * REQUEST METHOD が GET かを調べる
     * @return boolean
     */
    public function isGet()
    {
        if (\filter_input(\INPUT_SERVER, 'REQUEST_METHOD') === 'GET') {
            return \TRUE;
        } else {
            return \FALSE;
        }
    }

    /**
     * REQUEST METHOD が POST かを調べる
     * @return boolean
     */
    public function isPost()
    {
        if (\filter_input(\INPUT_SERVER, 'REQUEST_METHOD') === 'POST') {
            return \TRUE;
        } else {
            return \FALSE;
        }
    }

    /**
     * REQUEST URI を取得する
     * @return string
     */
    public function getRequestUri()
    {
        return \filter_input(\INPUT_SERVER, 'REQUEST_URI');
    }

    /**
     * フォームで入力された値を配列として取得する
     * 
     * @param array $args
     * @return array
     */
    public function getInputArray(array $args)
    {
        $request = array();
        if ($this->isGet()) {
            $request = \filter_input_array(\INPUT_GET, $args, \TRUE);
        } else {
            $request = \filter_input_array(\INPUT_POST, $args, \TRUE);
        }
        return $request;
    }

}
