<?php

namespace TeaBreak;

require_once 'Smarty.class.php';

/**
 * Viewクラス
 * @author Ken-ichiro, Takahashi <ken_takahashi@yahoo.co.jp>
 */
class ViewHandler extends \Smarty
{

    public function __construct()
    {
        parent::__construct();

        $config = \TeaBreak\ConfigurationHandler::getInstance();

        $this->addTemplateDir($config->template->template_dir);
        $this->addPluginsDir($config->template->plugins_dir);

        require_once 'TeaBreak\Plugins\LoadDataPlugin.php';
        $loadDataPlugin = new Plugins\LoadDataPlugin();
        $this->registerPlugin('function', 'loadData', array($loadDataPlugin, 'pluginMethod'));
    }

    public function showErrorPage404(Exception\PageNotFoundException $e) {
        $tpl = $this->createTemplate('404.tpl');
        $tpl->assign('message', $e->getMessage());
        $tpl->assign('stack_trace', $e->getTraceAsString());
        $tpl->display();
    }
}
