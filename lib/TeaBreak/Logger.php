<?php

namespace TeaBreak;

/**
 * logging module
 */
class Logger
{

    /**
     * message type 0
     * php.ini の error_log 依存
     */
    const TYPE_DEFAULT = 0;

    /**
     * message type 1 
     * メール送信
     */
    const TYPE_MAIL = 1;

    /**
     * massage type 2 
     * ファイル出力
     */
    const TYPE_FILE = 3;

    /**
     * message type 4 
     * SAPIへ出力
     */
    const TYPE_SAPI = 4;

    /**
     * log level 0 emerg
     * システムが使用不能
     */
    const LOG_EMERG = 0;

    /**
     * log level 1 alert
     * 直ちに対処が必要
     */
    const LOG_ALERT = 1;

    /**
     * log level 2 crit
     * 致命的な状態
     */
    const LOG_CRIT = 2;

    /**
     * log level 3 ERR
     * エラー状態
     */
    const LOG_ERR = 3;

    /**
     * log level 4 warning
     * 警告状態
     */
    const LOG_WARNING = 4;

    /**
     * log level 5 notice
     * 通常状態だが大事な情報
     */
    const LOG_NOTICE = 5;

    /**
     * log level 6 info
     * 通知
     */
    const LOG_INFO = 6;

    /**
     * log level 7 debug
     * デバッグレベルの情報
     */
    const LOG_DEBUG = 7;

    /**
     * message type
     * @var int
     */
    protected static $_message_type = self::TYPE_DEFAULT;

    /**
     * destination
     * @var string
     */
    protected static $_destination = null;

    /**
     * extra headers (message type が 2 の時のみ利用可能)
     * @var string
     */
    protected static $_extra_headers = null;

    /**
     * log level
     * @var int
     */
    protected static $_log_level = self::LOG_INFO;

    /**
     * message type を設定する
     * デフォルトは 0
     * php.ini の error_log の設定に依存する
     * @param type int
     * @throws \InvalidArgumentException
     */
    public static function setMessageType($type)
    {
        if(!is_int($type)) {
            throw new \InvalidArgumentException('invalid message type: invalid valiable type: ' . gettype($type));
        }
        
        switch ($type) {
            case static::TYPE_DEFAULT:
            case static::TYPE_MAIL:
            case static::TYPE_FILE:
            case static::TYPE_SAPI:
                static::$_message_type = $type;
                break;

            default:
                throw new \InvalidArgumentException('invalid message type: ' . $type);
        }
        
        return TRUE;
    }

    /**
     * ログの保存先・送信先メールアドレス
     * message type が 1, 3 の場合のみ設定可能
     * @param string$dest
     * @throws \InvalidArgumentException
     */
    public static function setDestination($dest)
    {
        switch (static::$_message_type) {
            case static::TYPE_MAIL:
                static::$_destination = static::_checkMail($dest);
                break;

            case static::TYPE_FILE:
                static::$_destination = static::_checkDir($dest);
                break;

            default:
                throw new \InvalidArgumentException('invalid destination: unable message type');
        }
        
        return TRUE;
    }

    /**
     * 追加ヘッダ
     * message type が 1 の場合のみ設定可能
     * @param string $ext
     * @throws \InvalidArgumentException
     */
    public static function setExtraHeaders($ext)
    {
        if (static::$_message_type !== static::TYPE_MAIL) {
            throw new \InvalidArgumentException('invalid extra headers: unabel message type');
        }

        static::$_extra_headers = $ext;
        
        return TRUE;
    }

    /**
     * log level
     * デフォルトは 6 info
     * @param int $level
     * @throws \InvalidArgumentException
     */
    public static function setLogLevel($level)
    {
        if (is_int($level) && 0 <= $level && $level <= 7) {
            static::$_log_level = $level;
        } else {
            throw new \InvalidArgumentException('invalid log level: ' . $level);
        }
        
        return TRUE;
    }

    /**
     * ログ出力
     * ログレベルにかかわらずログ出力する
     * @param string $message
     * @param int $type
     * @param string $dest
     * @param string $ext
     * @throws \InvalidArgumentException
     */
    public static function log($message, $type = null, $dest = null, $ext = null)
    {

        $message_type = empty($type) ? static::$_message_type : $type;

        switch ($message_type) {
            case static::TYPE_MAIL:
                if (!empty($dest)) {
                    $destination = static::_checkMail($dest);
                } else {
                    $destination = static::$_destination;
                }

                error_log($message, $message_type, $destination, $ext);
                break;

            case static::TYPE_FILE:
                if (!empty($dest)) {
                    $destination = static::_checkDir($dest);
                } else {
                    $destination = static::$_destination;
                }

                error_log($message, $message_type, $destination);
                break;

            default:
                error_log($message, $message_type);
        }
        
        return TRUE;
    }

    /**
     * システムが使用不能レベルのログ
     * @param string $message
     * @param int $type
     * @param string $dest
     * @param string $ext
     * @throws \InvalidArgumentException
     */
    public static function emerg($message, $type = null, $dest = null, $ext = null)
    {
        return static::log($message, $type, $dest, $ext);
    }

    /**
     * 直ちに対処が必要なレベルのログ
     * @param string $message
     * @param int $type
     * @param string $dest
     * @param string $ext
     * @throws \InvalidArgumentException
     */
    public static function alert($message, $type = null, $dest = null, $ext = null)
    {
        if (static::$_log_level >= static::LOG_ALERT) {
            return static::log($message, $type, $dest, $ext);
        }
        
        return TRUE;
    }

    /**
     * エラー状態レベルのログ
     * @param string $message
     * @param int $type
     * @param string $dest
     * @param string $ext
     * @throws \InvalidArgumentException
     */
    public static function err($message, $type = null, $dest = null, $ext = null)
    {
        if (static::$_log_level >= static::LOG_ERR) {
            return static::log($message, $type, $dest, $ext);
        }
        
        return TRUE;
    }

    /**
     * 警告状態レベルのログ
     * @param string $message
     * @param int $type
     * @param string $dest
     * @param string $ext
     * @throws \InvalidArgumentException
     */
    public static function warning($message, $type = null, $dest = null, $ext = null)
    {
        if (static::$_log_level >= static::LOG_WARNING) {
            return static::log($message, $type, $dest, $ext);
        }
        
        return TRUE;
    }

    /**
     * 通常状態だが大事な情報レベルのログ
     * @param string $message
     * @param int $type
     * @param string $dest
     * @param string $ext
     * @throws \InvalidArgumentException
     */
    public static function notice($message, $type = null, $dest = null, $ext = null)
    {
        if (static::$_log_level >= static::LOG_NOTICE) {
            return static::log($message, $type, $dest, $ext);
        }
        
        return TRUE;
    }

    /**
     * 通知ログ
     * @param string $message
     * @param int $type
     * @param string $dest
     * @param string $ext
     * @throws \InvalidArgumentException
     */
    public static function info($message, $type = null, $dest = null, $ext = null)
    {
        if (static::$_log_level >= static::LOG_INFO) {
            return static::log($message, $type, $dest, $ext);
        }
        
        return TRUE;
    }

    /**
     * デバッグログ
     * @param string $message
     * @param int $type
     * @param string $dest
     * @param string $ext
     * @throws \InvalidArgumentException
     */
    public static function debug($message, $type = null, $dest = null, $ext = null)
    {
        if (static::$_log_level >= static::LOG_DEBUG) {
            return static::log($message, $type, $dest, $ext);
        }
        
        return TRUE;
    }

    /**
     * ディレクトリの存在チェック
     * @param string $dest
     * @return string
     * @throws \InvalidArgumentException
     */
    protected static function _checkDir($dest)
    {
        if (is_string($dest)) {
            $dir = basename($dest);
            if (is_dir($dir)) {
                return $dest;
            }
        }

        throw new \InvalidArgumentException('invalid destination: ' . $dest);
    }

    /**
     * メールアドレスの妥当性チェック
     * @param string $dest
     * @return string
     * @throws \InvalidArgumentException
     */
    protected static function _checkMail($dest)
    {
        $pattern = <<<__END_OF_PATTERN__
/^(?:(?:(?:(?:[a-zA-Z0-9_!#\$\%&'*+/=?\^`{}~|\-]+)(?:\.(?:[a-zA-Z0-9_!#\$\%&'*+/=?\^`{}~|\-]+))*)|(?:"(?:\\[^\r\n]|[^\\"])*")))\@(?:(?:(?:(?:[a-zA-Z0-9_!#\$\%&'*+/=?\^`{}~|\-]+)(?:\.(?:[a-zA-Z0-9_!#\$\%&'*+/=?\^`{}~|\-]+))*)|(?:\[(?:\\\S|[\x21-\x5a\x5e-\x7e])*\])))$/
__END_OF_PATTERN__;
        if (!preg_match($pattern, $dest)) {
            throw new \InvalidArgumentException('invalid destination: ' . $dest);
        }
        return $dest;
    }

}
