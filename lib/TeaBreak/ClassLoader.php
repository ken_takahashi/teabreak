<?php

namespace TeaBreak;

/**
 * クラスオートローダー
 */
class ClassLoader
{

    /**
     * ライブラリサーチパス
     * @var array
     */
    protected $_dirs;

    /**
     * コンストラクタ
     */
    public function __construct()
    {
        \spl_autoload_register(array($this, 'loader'));
    }

    /**
     * ライブラリのサーチパスを追加する
     * @param string $dir
     */
    public function registerDir($dir)
    {
        $realpath = \realpath($dir);
        if ($realpath) {
            $this->_dirs[] = $realpath;
        }
    }

    /**
     * __autoloader から呼び出されるメソッド
     * @param  string $className
     * @return void
     * @throws \Exception
     */
    public function loader($className)
    {
        if (\preg_match('/^TeaBreak/', $className) === 0) {
            return;
        }

        foreach ($this->_dirs as $dir) {
            if (\is_dir($dir) === false) {
                continue;
            }
            $classFile = \realpath($dir . \DIRECTORY_SEPARATOR . \str_replace('\\', \DIRECTORY_SEPARATOR, $className . '.php'));
            if ($classFile) {
                include $classFile;

                return;
            }
        }
    }

}
