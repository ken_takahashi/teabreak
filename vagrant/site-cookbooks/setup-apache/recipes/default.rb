bash "Create Symbolic Link" do
    cwd "/etc/httpd"
    user "root"
    code <<-EOH
ln -s /etc/httpd/sites-available/default /etc/httpd/sites-enabled/default
    EOH
end
