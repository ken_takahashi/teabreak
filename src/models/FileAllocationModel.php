<?php

class FileAllocationModel extends \TeaBreak\Database\DataModel
{

    const TABLE_NAME = 'file_allocation';

    protected static $_schema = array(
        'id' => array(
            'type' => 'BigInt',
            'notNull' => true,
            'unique' => true,
            'unsigned' => true,
        ),
        'name' => array(
            'type' => 'Varchar',
            'notNull' => true,
        ),
        'path' => array(
            'type' => 'Varchar',
            'notNull' => true,
        ),
        'created' => array(
            'type' => 'DateTime',
            'defaultCurrentTimestamp' => true,
        ),
        'modified' => array(
            'type' => 'Timestamp',
            'defaultCurrentTimestamp' => true,
            'updateCurrentTimestamp' => true,
        ),
    );

}
