<?php

require_once APP_PATH . DIRECTORY_SEPARATOR . 'models/FileAllocationModel.php';

class FileAllocationMapper extends \TeaBreak\Database\DataMapper
{

    const MODEL_CLASS = 'FileAllocationModel';

    /**
     * フォームに入力された条件での検索
     * @param array $form
     * @return \PDOStatement
     */
    public function find(array $form)
    {
        if (empty($form)) {
            return $this->find_all();
        }

        // ファイル名での検索
        if (empty($form['name']) === FALSE) {
            $wheres[] = 'name >= :name';
            $query_params['name'] = $form['name'];
        }

        // 作成日での検索
        if (empty($form['from_date']) === FALSE) {
            try {
                $from_date = new \DateTime($form['from_date']);
                if ($from_date) {
                    $wheres[] = 'created >= :created_from';
                    $query_params['created_from'] = $from_date->format('U');
                }
            } catch (Exception $e) {
                // pass;
            }
        }
        if (empty($form['to_date']) === FALSE) {
            try {
                $to_date = new \DateTime($form['to_date']);
                if ($to_date) {
                    $wheres[] = 'created <= :created_to';
                    $query_params['created_to'] = $to_date->format('U');
                }
            } catch (Exception $e) {
                // pass;
            }
        }

        // クエリの組み立て
        $sql = 'select * from file_allocation';
        if ($wheres) {
            $sql .= ' where ' . implode(' AND ', $wheres);
        }

        // 検索
        $stmt = $this->_pdo->prepare($sql);
        $stmt->execute($query_params);
        return $this->_Ormapping($stmt);
    }

    /**
     * @param $entity
     */
    public function save($entity)
    {
        if (is_a($entity, static::MODEL_CLASS) === FALSE) {
            throw new InvalidArgumentException;
        }

        if (empty($entity->name) || empty($entity->path)) {
            throw new RuntimeException('path does not defined.');
        }

        $sql = 'INSERT INTO file_allocation (name, path, created, modified) VALUES (:name, :path, :created, :modified)';
        $stmt = $this->_pdo->prepare($sql);
        $stmt->bindParam('name', $entity->name);
        $stmt->bindParam('path', $entity->path);
        $stmt->bindParam('created', time());
        $stmt->bindParam('modified', time());
        $ret = $stmt->execute();
    }

    public function delete($entity)
    {
        
    }

}
