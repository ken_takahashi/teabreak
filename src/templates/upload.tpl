<html>
    <head>
        <title>upload</title>
    </head>
    <body>
{block name="body"}
{upload}
{if $error}
        <p>error: {$error}</p>
{else}
        <p>upload done: {$filename}</p>
{/if}
        <p><a href="/">back to top</a></p>
{/block}
    </body>
</html>