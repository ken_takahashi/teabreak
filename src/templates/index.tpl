<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Uploader</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/jumbotron.css" rel="stylesheet">

        <!-- Bootstrap Form Helpers -->
        <link href="css/datepicker3.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        {block name="body"}
            <h1>Uploader</h1>
            <div class="container">

                <div class="row">
                    <div class="col-md-6">
                        <h3>検索</h3>
                        <form method="post">
                            <div class="form-group">
                                <label>ファイル名</label>
                                <input class="form-control" type="text" name="name" size="64" value="{$smarty.request.name|default:''}">
                            </div>
                            <div class="form-group">
                                <label>作成日</label>
                                <div class="input-group input-daterange" id="datepicker">
                                    <input type="text" class="form-control" name="from_date" value="{$smarty.request.from_date|date_format:'%Y/%m/%d'}" />
                                    <span class="input-group-addon">～</span>
                                    <input type="text" class="form-control" name="to_date" value="{$smarty.request.to_date|date_format:'%Y/%m/%d'}"/>
                                </div>                                    
                            </div>
                            <input class="btn" type="submit" value="検索">
                        </form>
                    </div>                

                    <div class="col-md-6">
                        <h3>ファイルアップロード</h3>
                        <form method="post" action="upload" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>upload</label>
                                <div class="input-group">
                                    <input id="fileSelector" class="form-control" type="text" onclick="$('input[id=upfile]').click();">
                                    <span class="input-group-btn"><button type="button" class="btn" onclick="$('input[id=upfile]').click();">ファイル選択</button></span>
                                </div>
                            </div>
                            <input type="file" id="upfile" name="upfile" style="display:none;">
                            <input type="submit" class="btn">
                        </form>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h3>ファイルリスト</h3>
                        {findForm}
                        {loadData loadClass="FileAllocationMapper" assign="list" find=$findForm}
                        <ul class="list-ustyled">
                            {foreach $list as $file}
                                <li class="col-md-6">
                                    <h5>{$file->name}</h5>
                                    <p class="text-muted" style="font-size:small">{$file->path}</p>
                                    <p class="text-muted" style="font-size:x-small">{$file->created|date_format:'%Y/%m/%d %T'}</p>
                                    <p class="text-muted" style="font-size:x-small">{$file->modified|date_format:'%Y/%m/%d %T'}</p>
                                </li>
                            {/foreach}
                        </ul>
                    </div>
                </div>
            </div>
        {/block}

        <script src="js/jquery-2.0.3.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/locales/bootstrap-datepicker.ja.js"></script>
        <script type="text/javascript">
            <!--
// date picker
$('.input-daterange').datepicker({
    format: "yyyy/mm/dd",
    language: "ja",
    autoclose: true,
    todayHighlight: true
});

// file selector
$('input[id=upfile]').change(function() {
    $('#fileSelector').val($(this).val());
});
            // -->
        </script>

    </body>
</html>