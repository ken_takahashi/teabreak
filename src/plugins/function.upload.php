<?php

require_once 'models/FileAllocationMapper.php';
require_once 'TeaBreak/Database/DatabaseHandler.php';

function smarty_function_upload($params, $template)
{
    $obj = new FileUpload();

    $obj->pluginMethod($params, $template);
}

class FileUpload extends \TeaBreak\Plugins\DbPluginBase
{

    public function pluginMethod($params, $template)
    {
        $template->assign('filename', $_FILES['upfile']['name']);

        try {
            $dbconf = $this->_config->database->strage;
            $dbh = $this->_dbh[$dbconf->name];
            $fat_mapper = new FileAllocationMapper($dbh);
            $fat_entity = new FileAllocationModel();
            $fat_entity->name = $_FILES['upfile']['name'];
            $fat_entity->path = $_FILES['upfile']['tmp_name'];
            $fat_mapper->save($fat_entity);
        } catch (\RuntimeException $e) {
            $template->assign('error', 'file does not uploaded.' . $e->getMessage());
        } catch (\Exception $e) {
            $template->assign('error', $e->getMessage());
        }
    }

}
