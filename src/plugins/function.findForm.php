<?php

function smarty_function_findForm($params, $template)
{
    $definition = array(
        'name' => FILTER_SANITIZE_STRING,
        'from_date' => FILTER_SANITIZE_STRING,
        'to_date' => FILTER_SANITIZE_STRING,
    );
    
    $template->assign('findForm', $definition);
}
