<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author ke-takahashi
 */
# basic configuration settings
define('TEABREAK_HOME', dirname(__DIR__));
if (realpath(TEABREAK_HOME . DIRECTORY_SEPARATOR . 'src/config')) {
    define('TEABREAK_CONF', realpath(TEABREAK_HOME . DIRECTORY_SEPARATOR . 'src/config'));
}
define('SMARTY_DIR', realpath(TEABREAK_HOME . DIRECTORY_SEPARATOR . 'lib/Smarty-3.1.15/libs') . DIRECTORY_SEPARATOR);

# Library Path
$lib_path = realpath(TEABREAK_HOME . DIRECTORY_SEPARATOR . 'lib');
# Vendor Path
$vendor_path = realpath(TEABREAK_HOME . DIRECTORY_SEPARATOR . 'vendor');
# Application Path
$app_path = realpath(TEABREAK_HOME . DIRECTORY_SEPARATOR . 'tests');
define('APP_PATH', $app_path);

// set include path
$path_array = array($app_path, $lib_path, SMARTY_DIR, get_include_path());
set_include_path(implode(PATH_SEPARATOR, $path_array));

require_once realpath($vendor_path . DIRECTORY_SEPARATOR . 'autoload.php');

require_once 'TeaBreak/ClassLoader.php';
$loader = new TeaBreak\ClassLoader();
$loader->registerDir($lib_path);
