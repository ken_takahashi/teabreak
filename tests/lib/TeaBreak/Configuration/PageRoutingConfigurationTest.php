<?php

namespace TeaBreak\Configuration;

/**
 * Generated by PHPUnit_SkeletonGenerator 1.2.1 on 2013-11-15 at 22:14:12.
 */
class PageRoutingConfigurationTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var PageRoutingConfiguration
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->name = 'root';
        $this->conf = array(
            'access' => 'public',
            'type' => 'page',
            'template' => 'index.tpl'
        );
        $this->object = new PageRoutingConfiguration($this->name, $this->conf);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        
    }

    public function testObjectCreate()
    {
        $this->assertInstanceOf('TeaBreak\Configuration\PageRoutingConfiguration', $this->object);
    }

    public function testName()
    {
        $this->assertEquals($this->object->name, $this->name);
    }

    public function testAccess()
    {
        $this->assertEquals($this->object->access, $this->conf['access']);
    }

    public function testType()
    {
        $this->assertEquals($this->object->type, $this->conf['type']);
    }

    public function testTemplate()
    {
        $this->assertEquals($this->object->template, $this->conf['template']);
    }

}
