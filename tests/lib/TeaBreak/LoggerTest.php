<?php

namespace TeaBreak;

/**
 * Generated by PHPUnit_SkeletonGenerator 1.2.1 on 2014-01-07 at 15:25:36.
 */
class LoggerTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var Logger
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        
    }

    /**
     * @covers TeaBreak\Logger::setMessageType
     * @todo   Implement testSetMessageType().
     */
    public function testSetMessageType()
    {
        $this->assertTrue(Logger::setMessageType(0));
        $this->assertTrue(Logger::setMessageType(1));
        $this->assertTrue(Logger::setMessageType(3));
        $this->assertTrue(Logger::setMessageType(4));
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testSetMessageTypeExceptionAscii()
    {
        Logger::setMessageType('a');
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testSetMessageTypeExceptionUnderRange()
    {
        Logger::setMessageType(-1);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testSetMessageTypeExceptionUpperRange()
    {
        Logger::setMessageType(5);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testSetMessageTypeException2()
    {
        Logger::setMessageType(2);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testSetMessageTypeExceptionMultibyte()
    {
        Logger::setMessageType('あ');
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testSetMessageTypeExceptionNull()
    {
        Logger::setMessageType(null);
    }

    /**
     * @covers TeaBreak\Logger::setDestination
     * @todo   Implement testSetDestination().
     */
    public function testSetDestination()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers TeaBreak\Logger::setExtraHeaders
     * @todo   Implement testSetExtraHeaders().
     */
    public function testSetExtraHeaders()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers TeaBreak\Logger::setLogLevel
     * @todo   Implement testSetLogLevel().
     */
    public function testSetLogLevel()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers TeaBreak\Logger::log
     * @todo   Implement testLog().
     */
    public function testLog()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers TeaBreak\Logger::emerg
     * @todo   Implement testEmerg().
     */
    public function testEmerg()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers TeaBreak\Logger::alert
     * @todo   Implement testAlert().
     */
    public function testAlert()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers TeaBreak\Logger::err
     * @todo   Implement testErr().
     */
    public function testErr()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers TeaBreak\Logger::warning
     * @todo   Implement testWarning().
     */
    public function testWarning()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers TeaBreak\Logger::notice
     * @todo   Implement testNotice().
     */
    public function testNotice()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers TeaBreak\Logger::info
     * @todo   Implement testInfo().
     */
    public function testInfo()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

    /**
     * @covers TeaBreak\Logger::debug
     * @todo   Implement testDebug().
     */
    public function testDebug()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
                'This test has not been implemented yet.'
        );
    }

}
