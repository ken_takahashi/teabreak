<?php

# basic configuration settings
define('TEABREAK_HOME', dirname(__DIR__));
if(realpath(TEABREAK_HOME . DIRECTORY_SEPARATOR . 'src')) {
    define('APP_PATH', TEABREAK_HOME . DIRECTORY_SEPARATOR . 'src');
    define('TEABREAK_CONF', realpath(APP_PATH . DIRECTORY_SEPARATOR . 'config/app.yaml'));
}

try {
    require_once APP_PATH . DIRECTORY_SEPARATOR . 'working_report_uploader.php';
    $app = new working_report_uploader();
    $app->main();
} catch (\Exception $e) {
    echo "<h1>Fatal Error !</h1>";
    echo "<p>" . $e->getMessage() . "</p>\n";
    echo "<p>" . $e->getFile() . "(" . $e->getLine() . ")</p>\n";
    echo "<pre>" . $e->getTraceAsString() . "</pre>\n";
}